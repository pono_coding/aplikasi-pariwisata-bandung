-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2020 at 08:47 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ridwan`
--

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `waktu_upload` datetime NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id`, `nama`, `foto`, `deskripsi`, `waktu_upload`, `id_user`) VALUES
(8, 'Kiara Artha Park, Destinasi Instagramable Terbaru di Kota Bandung. Eh Ada Kampung Korea Juga Lho!', 'kiara_park.jpg', '<p>Bandung nggak pernah kehabisan ide buat memanjakan pengunjung ke kota tersebut. Sudah ada banyak pilihan wisata ataupun nongkrong di kota kembang, mulai dari Lembang di utara hingga Ciwidey di selatan. Spot wisata hits banyak, tempat nongkrong lucu pun nggak kehitung jumlahnya. Pokoknya Bandung memang kotanya anak muda banget deh. Lengkap dan memanjakan traveler yang datang ke sana!</p>\r\n\r\n<p><ins><img alt=\"\" src=\"https://wtf2.forkcdn.com/www/delivery/lg.php?bannerid=0&amp;campaignid=0&amp;zoneid=6244&amp;loc=https%3A%2F%2Fwww.hipwee.com%2Ftravel%2Fkampung-korea-di-bandung%2F&amp;referer=https%3A%2F%2Fwww.hipwee.com%2Ftag%2Fwisata-bandung%2F&amp;cb=d8b5f96ab7\" style=\"height:0px; width:0px\" /></ins></p>\r\n\r\n<p>Nah ada satu lagi nih destinasi wisata yang bakal tambah bikin kamu betah liburan di Bandung, yaitu Kiara Artha Park. Destinasi ini adalah tempat wisata terpadu karena di sini terdapat danau buatan, air mancur yang menari-nari indah, taman-taman yang cantik dan menarik dan nggak ketinggalan ada kampung korea di sana. Wah banyak banget tuh wahananya. Yuk simak aja ulasan Hipwee Travel.</p>\r\n\r\n<h3>Sudah dibangun sejak lama, Kiara Artha Park yang berlokasi di Jl. Banten, Kebonwaru, Kec. Batununggal, Kota Bandung ini jadi pilihan baru warga Bandung yang ingin berlibur</h3>\r\n\r\n<p><img alt=\"\" src=\"https://cdn-image.hipwee.com/wp-content/uploads/2019/10/hipwee-67798584_1743251522484908_6015412082575205499_n-750x938.jpg\" style=\"height:938px; width:750px\" /></p>\r\n\r\n<p>kiara artha park via&nbsp;<a href=\"https://www.instagram.com/p/B1VMw6TFKK8/\" target=\"new\">www.instagram.com</a></p>\r\n\r\n<h3>Soft launching Kiara Artha Park dilakukan pada tanggal 17 Agustus 2019. Sebenarnya taman ini belum dibuka secara resmi, rencana di akhir tahun ini. Meski begitu pengunjung sudah banyak yang datang ke sana</h3>\r\n\r\n<p><img alt=\"\" src=\"https://cdn-image.hipwee.com/wp-content/uploads/2019/10/hipwee-73446513_174562513718572_5113083917466800665_n-750x938.jpg\" style=\"height:938px; width:750px\" /></p>\r\n\r\n<p>tempatnya asik ya via&nbsp;<a href=\"https://www.instagram.com/p/B3x_1YoA9jX/\" target=\"new\">www.instagram.com</a></p>\r\n\r\n<h3>Jika malam hari, Kiara Artha Park ini sangat gemerlapan dan tampak cantik untuk dipotret kamera. Bakalan betah di sana berlama-lama deh</h3>\r\n', '2019-12-02 09:01:41', 1),
(9, 'Selain Usir Stres, Ini 6 Manfaat Liburan untuk Kesehatan Mental Kita', 'manfaat.jpg', '<p>&nbsp;Liburan alias traveling bisa jadi pilihan tepat untuk menyegarkan pikiran ketika stres dan rasa jenuh melanda. Kegiatan traveling bahkan tak hanya ampuh mengurangi stres, tapi juga membawa manfaat bagi kesehatan mental Anda. Apa saja?</p>\r\n\r\n<p>Bagi Anda yang menghabiskan belasan jam untuk bekerja setiap hari, cobalah menyempatkan diri untuk menikmati liburan sejenak.</p>\r\n\r\n<p>Berlibur memberikan Anda kesempatan untuk melakukan kegiatan yang Anda gemari. Selain itu, melakukan traveling bisa membawa manfaat yang menguntungkan kondisi mental Anda karena Anda melakukannya bebas dari beban.</p>\r\n\r\n<p>Berikut adalah beberapa manfaat liburan yang bisa Anda dapatkan, cekidot :</p>\r\n\r\n<p>1. Memberikan kepuasan dan rasa bahagia</p>\r\n\r\n<p>Berlibur dapat memberikan kepuasan dari segi fisik maupun psikologis. Dengan liburan, Anda mendapatkan kesimbangan antara pekerjaan dan kehidupan sehari-hari. Anda pun tidak dihadapkan pada tekanan rutinitas selama menikmati waktu liburan.</p>\r\n\r\n<p>Semua hal tersebut membuat Anda merasakan kepuasan dan rasa bahagia.</p>\r\n\r\n<p>Bahkan, tak hanya saat liburan, emosi positif tersebut juga bisa bertahan hingga berminggu-minggu kemudian. Hasilnya, kesehatan mental Anda jauh lebih baik.</p>\r\n\r\n<p><img alt=\"\" src=\"https://bandungkita.id/wp-content/uploads/2019/08/liburan-2.jpg\" style=\"height:556px; width:830px\" /></p>\r\n\r\n<p>Ilustrasi liburan (foto:net)</p>\r\n\r\n<p>2. Menjadi lebih produktif</p>\r\n\r\n<p>Stres berkepanjangan yang berasal dari pekerjaan dan kehidupan sehari-hari dapat menghambat kinerja otak. Akibatnya, produktivitas menurun dan Anda kesulitan untuk fokus.</p>\r\n\r\n<p>Traveling dapat mengurangi stres sehingga Anda bisa tetap produktif saat bekerja. Selain itu, rasa bahagia dari traveling juga akan meningkatkan kinerja otak dan tentunya berdampak positif pada pekerjaan Anda.</p>\r\n\r\n<p>Hal ini dibuktikan dalam surevi yang dimuat dalam Harvard Business Review. Studi tersebut menyebutkan karyawan yang berlibur ternyata bisa menyelesaikan pekerjaannya dengan lebih mudah.</p>\r\n\r\n<p>Mereka pun bekerja lebih produktif dibandingkan rekan-rekannya yang tidak berlibur.</p>\r\n\r\n<p>BACA JUGA :</p>\r\n\r\n<h2><a href=\"https://bandungkita.id/2019/06/06/ini-9-tempat-wisata-di-bandung-raya-rekomendasi-bandungkita-id-yang-wajib-dikunjungi-saat-libur-lebaran/\">Ini 9 Tempat Wisata di Bandung Raya Rekomendasi BandungKita.id yang Wajib Dikunjungi Saat Libur Lebaran</a></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>3. Menurunkan risiko gangguan psikologis</p>\r\n\r\n<p>Salah satu manfaat traveling bagi kesehatan mental adalah mengurangi risiko depresi dan gangguan kecemasan. Pasalnya, kegiatan ini menjauhkan Anda dari orang-orang dan aktivitas yang memicu produksi hormon kortisol penyebab stres.</p>\r\n\r\n<p>Anda bahkan tak harus berlibur panjang untuk bisa memperoleh manfaat ini. Liburan singkat selama tiga hari saja sudah dapat menurunkan hormon stres dan membuat pikiran Anda terasa jernih kembali.</p>\r\n\r\n<p>4. Meningkatkan kreativitas</p>\r\n\r\n<p>Selama liburan, Anda akan menjumpai orang-orang baru, budaya yang berbeda, bahkan makanan baru yang belum pernah Anda temui sebelumnya. Semua pengalaman ini membuat wawasan Anda menjadi lebih luas dan beragam.</p>\r\n\r\n<p>Wawasan luas yang berasal dari pengalaman liburan akhirnya dapat memunculkan inspirasi baru. Inilah yang menjadi cikal bakal kreativitas pada orang-orang yang gemar pergi berlibur.</p>\r\n\r\n<p>BACA JUGA :</p>\r\n\r\n<h2><a href=\"https://bandungkita.id/2019/08/09/5-pantai-di-garut-ini-wajib-dikunjungi-karena-pesona-eksotisnya/\">5 Pantai di Garut Ini Wajib Dikunjungi Karena Pesona Eksotisnya</a></h2>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>5. Membuat Anda lebih bersemangat</p>\r\n\r\n<p>Salah satu penelitian menemukan bahwa tiga hari setelah berlibur, para responden mengalami peningkatan kualitas tidur dan mood.</p>\r\n\r\n<p>Mereka juga menjadi lebih bersemangat, jarang mengeluh, dan mengalami peningkatan kemampuan berpikir.</p>\r\n\r\n<p>Efek ini berlangsung hingga 5 minggu kemudian, terutama pada responden yang banyak menghabiskan waktu untuk dirinya sendiri selama liburan.</p>\r\n\r\n<p>6. Memperkuat hubungan dengan orang lain</p>\r\n\r\n<p>Menjalani liburan bersama atau berbagi pengalaman liburan dapat memperkuat hubungan Anda dengan orang lain. Pengalaman unik atau buruk sekali pun bahkan bisa menjadi topik perbincangan yang menarik bersama orang lain.</p>\r\n\r\n<p>Kegiatan ini juga membuat Anda semakin memahami orang lain dan memperperat ikatan emosional yang telah terjalin. Terlebih lagi jika Anda berlibur bersama orang terdekat seperti pasangan atau keluarga.</p>\r\n\r\n<p>Liburan nyatanya lebih dari sekadar kegiatan menyenangkan di tengah sibuknya pekerjaan. Melakukan traveling minimal satu kali setiap tahun akan memberikan begitu banyak manfaat bagi kesehatan mental dan produktivitas Anda.</p>\r\n\r\n<p>Agar manfaatnya menjadi lebih optimal, pastikan Anda mempersiapkan seluruh kebutuhan liburan dengan baik. Dengan begitu, liburan Anda tidak akan terganggu oleh berbagai hal yang tidak direncanakan.(*)</p>\r\n', '2019-12-02 09:05:49', 1),
(10, 'Anugrah Budaya', 'anugrah_budaya.jpeg', '<p>Senin (25/11) bertempat di Grand Ballroom Hotel Savoy Homann diadakan Malam Anugerah Budaya Kota Bandung 2019. Anugerah Budaya Kota Bandung merupakan agenda tahunan yang dilaksanakan Disbudpar Kota Bandung sebagai bentuk apresiasi atas karya seniman dan budayawan yang telah mengharumkan nama Kota Bandung tercinta.</p>\r\n\r\n<p>Kegiatan ini telah berlangsung dari tahun 2006 dan telah melahirkan penerima anugerah budaya sebanyak 155 orang. Anugerah Budaya Kota Bandung tahun 2019 mengusung tema &ldquo;Mere Dangiang, Mawa Bandung Kajauhna&rdquo;</p>\r\n\r\n<p>Penyeleksian calon penerima Anugerah Budaya Kota Bandung tahun ini dilaksanakan dalam 3 tahap yang telah dimulai dari tanggal 23 September 2019. Adapun dewan juri/ tim penilai untuk kegiatan ini berjumlah 5 orang yang terdiri dari berbagai unsur dan yang berkompeten pada bidangnya masing masing. Berikut adalah nama nama dewan juri:</p>\r\n\r\n<ol>\r\n	<li>Prof. Dr Arthur S Nalan, M. Hum sebagai Koordinator (Unsur Akademis Isbi dan Budayawan)</li>\r\n	<li>Dr.Ettirs,M.Hum sebagai Sekretaris (Unsur Budayawan)</li>\r\n	<li>Dr Suhendi Afryanto, S.Kar.,M.M sebagai Anggota (Unsur Akademis ISBI, Komposer dan Budayawan)</li>\r\n	<li>Dr. Tisna Sanjaya sebagai Anggota (Unsur Akademis ITB dan Seniman)</li>\r\n	<li>Dr.Tedi Permadi,M.Hum sebagai Anggota (Unsur Akademis UPI dan Filolog)</li>\r\n</ol>\r\n\r\n<p>Berikut nama-nama penerima Anugerah Budaya tahun 2019</p>\r\n\r\n<p>-Uko Hendarto (Pencipta Lagu Pop Sunda)</p>\r\n\r\n<p>-Galura Media (Bahasa Sunda)</p>\r\n\r\n<p>-Didi Tarsidi (Pengembang&nbsp; Pendidikan Inklusif)</p>\r\n\r\n<p>-Hawe Setiawan (Pemikir Kebudayaan)</p>\r\n\r\n<p>-Ine Arini Bastaman (Seniman Tari)</p>\r\n\r\n<p>-Wawan Sofwan (Seniman Teater)</p>\r\n\r\n<p>-Rosid (Seniman Lukis)</p>\r\n\r\n<p>-Batagor Isan (Makanan Tradisional)</p>\r\n\r\n<p>-Comonroom (Literasi Budaya)</p>\r\n\r\n<p>-Taufik Hidayat (Pelestari Saung Angklung Udjo)</p>\r\n\r\n<p>Selamat untuk para penerima Anugerah Budaya. Semoga kegiatan ini dapat lebih meningkatkan kecintaan dan semangat melestarikan kita terhadap budaya sendiri.</p>\r\n', '2019-12-03 04:09:14', 2),
(11, 'Peresmian Kampung Wisata Kreatif dan Co-Working Space Braga', 'Peresmian_Kampung_Wisata_Kreatif_dan_Co-Working_Space_Braga.jpeg', '<p>Rangkaian acara Braga Festival 2019 diawali dengan Launching Kampung Wisata Kreatif, Warisan Budaya dan Sejarah Braga serta Peresmian Co-Working Space Braga.<br />\r\nDiresmikan langsung oleh Wali Kota Bandung Oded M. Danial, Kampung Wisata Kreatif Braga merupakan kawasan wisata sejarah dan cagar budaya dengan konsep pariwisata berbasis masyarakat (community based tourism) dan pariwisata secara berkelanjutan (sustainable tourism).</p>\r\n\r\n<p>Beberapa kawasan yang dihadirkan di Kampung Wisata Kreatif Braga yaitu Kawasan Tanaman Obat Keluarga RW 06, Sanggar Melukis, Kacapi Suling, Tembang Sunda Cianjuran dan Degung di Rumah Seni Ropih serta Industri Kerajinan Daur Ulang Sampah Koran dan Kantong Plastik RW 06. Ada juga kawasan Pasar Antik Cikapundung dan destinasi wisata kuliner legendaris seperti Braga Permai, Kopi Aroma, dan Warung Kopi Purnama.</p>\r\n\r\n<p>Selain itu, Co-Working Space Braga pun turut diresmikan. Proyek percontohan pertama program Co-Working Space di Kota Bandung ini memiliki fungsi :</p>\r\n\r\n<ul>\r\n	<li>Menyediakan tempat bagi individu maupun komunitas untuk mengembangkan usaha pada tahap awal</li>\r\n	<li>Menyediakan fasilitas kantor yang bisa digunakan Bersama</li>\r\n	<li>Menyediakan layanan konsultasi bisnis dan profesi</li>\r\n	<li>Menyelenggarakan program skill development dan inkubasi bisnis secara sistematis, terjadwal dan terukur</li>\r\n	<li>Membangun iklim sinergi dan membantu akses individu maupun komunitas dengan jaringan bisnis dan profesional</li>\r\n</ul>\r\n\r\n<p>Hadirnya Kampung Wisata Kreatif dan Co-Working Space di Braga diharapkan dapat menjadi sumber ekonomi bagi masyarakat dan meningkatkan daya tarik wisatawan untuk berkunjung ke Kota Bandung.</p>\r\n', '2019-12-03 04:10:24', 2),
(12, ' Kegiatan Joint Promotion “Pameran Pesona Mitra Budaya dan Pariwisata”', 'Kegiatan_Joint_Promotion_“Pameran_Pesona_Mitra_Budaya_dan_Pariwisata”.jpeg', '<p>Disbudpar Kota Bandung menyelenggarakan kegiatan Joint Promotion &ldquo;Pameran Pesona Mitra Budaya dan Pariwisata&rdquo; di Glamour Level Mall Paris van Java. Kegiatan ini dibuka secara resmi oleh Asisten Perekonomian dan Pembangunan Setda Kota Bandung Eric M. Attauriq (mewakili Wali Kota Bandung).</p>\r\n\r\n<p>Pertama kali diselenggarakan, Pameran Pesona Mitra Budaya dan Pariwisata menjadi sebuah ruang bagi para stakeholders baik pemerintah maupun non pemerintah untuk memperkenalkan, mensosialisasikan sekaligus mempromosikan potensi dan produk unggulannya. Daftar stakeholders yang ikut serta meramaikan di antaranya Dinas Pariwisata Pemerintah Kota Surakarta, ISBI, POLBAN, STIE EKUITAS, Telkom University, dan Pramuka Saka Pariwisata.</p>\r\n\r\n<p>Tidak hanya pameran, para pengunjung pun disuguhi pertunjukan kesenian menarik seperti tari tradisional dan musik @sambasunda. Ada juga Talkshow &ldquo;Strategi Pengembangan dan Pemasaran Destinasi Pariwisata di Era Industri 4.0&rdquo; dengan narasumber :<br />\r\n1. Dr. H. Wawan Gunawan, S.Sn., MM, Asisten Deputi Pengembangan Destinasi Regional II Bidang Pengembangan Destinasi Pariwisata;<br />\r\n2. Dra. Dewi Kaniasari, S.Sos., MA, Kepala Dinas Kebudayaan dan Pariwisata Kota Bandung</p>\r\n\r\n<p>Kegiatan ini diharapkan dapat menjadi awal yang baik untuk penyelenggaraan kegiatan Joint Promotion di tahun-tahun berikutnya.</p>\r\n', '2019-12-03 04:11:24', 2),
(13, 'Bandung Design Action Ke-7 Kembali Diselenggarakan', 'Bandung_Design_Action_Ke-7_Kembali_Diselenggarakan.jpeg', '<p>Kegiatan Design Action kembali diselenggarakan untuk ketujuh kalinya pada tanggal 31 Oktober &ndash; 1 November 2019 di Pendopo Kota Bandung. Kegiatan kolaborasi dengan Bandung Creative City Forum (BCCF) ini merupakan sebuah workshop tahunan dengan konsep &ldquo;Design Thinking&rdquo; untuk menemukan solusi inovatif bagi permasalahan dan tantangan Kota Bandung.&nbsp;</p>\r\n\r\n<p>Kali ini Design Action menghadirkan tema &ldquo;Inclusicity&rdquo; yang mengajak warga untuk melihat indahnya inklusivitas, memastikan potensi maupun kekayaan yang dimiliki Kota Bandung untuk dapat dinikmati dan diakses oleh semua warganya. Sebanyak 250 peserta yang mengikuti Design Action terdiri dari unsur akademisi, swasta, komunitas/asosiasi, pemerintah, media, dan masyarakat.</p>\r\n\r\n<p>Pada hari pertama pelaksanaan, para peserta diajak melakukan perjalanan untuk melihat lebih dekat seputar Kota Bandung dan merasakan interaksi dengan berbagai kalangan. Dibentuk sekitar 10 kelompok kecil untuk memetakan hasil perjalanan observasi melalui peta empati dan proses ideasi. Setelah itu di hari kedua masing-masing kelompok membuat sebuah prototipe dari ide solusi yang akan dikembangkan.</p>\r\n\r\n<p>Pemenang &quot;The Best Idea&quot; diberikan kepada Kelompok 5 dengan sub tema pembahasan&nbsp; &quot;REWOG RW06 Braga Kampung Kreatif dengan Mengembangkan Potensi Lokal&quot;.&nbsp;</p>\r\n\r\n<p>Diharapkan dengan kegiatan ini akan menjadikan Kota Bandung sebagai pelopor penyebarluasan ide dan proses untuk 160 Kota Kreatif di Indonesia serta adanya replikasi di 30 kecamatan Kota Bandung.</p>\r\n', '2019-12-03 04:12:30', 2),
(14, 'Sosialisasi Perda Pengelolaan Cagar Budaya', 'Sosialisasi_Perda_Pengelolaan_Cagar_Budaya.jpeg', '<p><strong>Sosialisasi Perda Pengelolaan Cagar Budaya</strong></p>\r\n\r\n<p>Senin, 28 Oktober 2019 Dinas Kebudayaan dan Pariwisata Kota Bandung mengadakan Sosialisasi Perda Pengelolaan Cagar Budaya tingkat Dinas sampai Kelurahan. Sosialisasi kali ini diadakan di Gedung Indonesia Menggugat yang juga merupakan salah satu bangunan cagar budaya Gol. A di kota Bandung.</p>\r\n\r\n<p>Sosialisasi Perda Cagar Budaya ini merupakan sosialisasi yang ketiga setelah yang pertama dilaksanakan bersama Himpunan Mahasiswa dari berbagai Universitas di kota Bandung, dan yang kedua dilaksanakan bersama pengelola dan pemilik gedung cagar budaya di kota Bandung. Sosialisasi Perda Pengelolaan Cagar Budaya hari ini dihadiri oleh Wakil Walikota Bandung, Bapak Yana Mulyana, perwakilan OPD, Camat, dan Lurah di kota Bandung.</p>\r\n\r\n<p>Para pemateri hari ini yaitu R. Rizky A Adiwilaga, SH selaku Konsultan &amp; Praktisi Hukuk HKI yang menyampaikan materi mengenai Aspek Hukum Pelestarian Cagar Budaya, Dr. Ir. Harastoeti, MSA selaku Ketua Tim Ahli Cagar Budaya yang menyampaikan materi mengenai Landasan Filosofi Pelestarian dan Sejarah Cagar Budaya, dan Ir. David B. Soediono &amp; Dr. Ir. Denny Zulkaidi selaku Tim Ahli Cagar Budaya yang menyampaikan materi mengenai Pokok Perda No.7/2018 dan Pengelolaan Cagar Budaya. Sosialisasi hari ini dimoderatori oleh Drs. Ipong Witono.</p>\r\n\r\n<p>Diharapkan dengan adanya sosialisasi kepada unsur kewilayahan, masing masing unsur kewilayahan dapat menjadi pihak pertama dalam mengidentifikasi bagunan, struktur, situs, kawasan cagar budaya yang berada di wilayah masing-masing. Selain itu, para unsur kewilayahan diharapkan juga dapat meneruskan sosialisai ke masyarakat, dan ikut memonitor bangungan-bangunan cagar budaya yang berada di wilayah masing-masing.</p>\r\n\r\n<p>Untuk upaya sosialisai Cagar Budaya kedepannya, Dinas Kebudayaan dan Pariwisata kota Bandung akan melakukan pemasangan stiker pada bangunan-bangunan cagar budaya dan launching aplikasi cagar budaya yang direncanakan terlaksana pada pertengahan bulan November mendatang. Selain itu, Disbudpar kota Bandung juga akan menggelar Penghargaan Cagar Budaya untuk memotivasi para pemilik dan pengelola bangunan cagar budaya agar terus memelihara, dan memanfaatkan bangunan-bangunan cagar budaya di kota Bandung.</p>\r\n', '2019-12-03 04:14:23', 2),
(15, 'Anugerah Pesona Pariwisata 2019', 'Anugerah_Pesona_Pariwisata_2019.jpeg', '<p>Malam penganugerahan Pesona Pariwisata 2019 telah diselenggarakan di Trans Studio Bandung (16/10). Dihadiri langsung oleh Wakil Wali Kota Bandung Yana Mulyana, Anugerah Pesona Pariwisata (APP) adalah salah satu bentuk kepedulian Pemerintah Kota Bandung dalam membangun kualitas pariwisata. Ajang penghargaan ini juga sebagai media konsolidasi dan evaluasi penyelenggaraan industri pariwisata sekaligus memberikan penghargaan bagi insan pariwisata Kota Bandung.</p>\r\n\r\n<p>Terdapat 39 nominator insan pariwisata yang bersaing dalam 13 kategori nominasi utama APP 2019 di antaranya Hotel Bintang 5, Hotel Bintang 4, Hotel Bintang 3, Hotel Bintang 2, Hotel Bintang 1, Hotel Non-Bintang, Restoran, Rumah Makan, Waralaba Makanan, Hiburan Karaoke, Usaha Spa, Biro Perjalanan Wisata Umum, dan Biro Perjalanan Wisata Haji dan Umrah. Ada juga 2 penghargaan khusus yang diberikan untuk kategori Pusat Perbelanjaan dan Destinasi Umum.</p>\r\n\r\n<p>Unsur-unsur yang dinilai meliputi tertib dan taat administrasi Peraturan Pemerintah dan Daerah ; pelaksanaan konsepsi sapta pesona ; keamanan dan kebersihan ; serta kreativitas yang dimiliki pengusaha kepariwisataan sebagai penambah ciri khasnya. Adapun yang terlibat sebagai tim penilai dalam penjurian APP 2019 antara lain BPPD Kota Bandung (Badan Pengelolaan Pendapatan Daerah), Satpol PP, Dinas Kebakaran dan Penanggulangan Bencana Kota Bandung, Dinas Ketenagakerjaan Kota Bandung, DPMTSP Kota Bandung (Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu), PD Kebersihan Kota Bandung, PDAM Kota Bandung, ASITA Jabar (Asosiasi Perusahaan Perjalanan Wisata Indonesia), PHRI Jabar (Perhimpunan Hotel dan Restoran Indonesia), dan Sekolah Tinggi Pariwisata Bandung.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Berikut adalah daftar pemenang Anugerah Pesona Pariwisata 2019 :</strong></p>\r\n\r\n<ul>\r\n	<li><strong>Hotel Bintang 5</strong></li>\r\n</ul>\r\n\r\n<p>The Trans Luxury Hotel | Jl. Gatot Subroto No.289</p>\r\n\r\n<ul>\r\n	<li><strong>Hotel Bintang 4</strong></li>\r\n</ul>\r\n\r\n<p>Grand Pasundan Convention Hotel | Jl. Peta No.147-149</p>\r\n\r\n<ul>\r\n	<li><strong>Hotel Bintang 3</strong></li>\r\n</ul>\r\n\r\n<p>Cipaku Garden Hotel | Jl. Cipaku Indah X No.2-4</p>\r\n\r\n<ul>\r\n	<li><strong>Hotel Bintang 2</strong></li>\r\n</ul>\r\n\r\n<p>Amaris Hotel Setiabudhi | Jl. Setiabudhi No.156A</p>\r\n\r\n<ul>\r\n	<li><strong>Hotel Bintang 1</strong></li>\r\n</ul>\r\n\r\n<p>POP! Hotel Festival Citylink | Jl. Peta No.241</p>\r\n\r\n<ul>\r\n	<li><strong>Hotel Non Bintang</strong></li>\r\n</ul>\r\n\r\n<p>Zodiak Hotel | Jl. Kebon Kawung No.54</p>\r\n\r\n<ul>\r\n	<li><strong>Restoran</strong></li>\r\n</ul>\r\n\r\n<p>Sindang Reret | Jl. Surapati No.55</p>\r\n\r\n<ul>\r\n	<li><strong>Rumah Makan</strong></li>\r\n</ul>\r\n\r\n<p>Ampera | Jl. Soekarno-Hatta No.618</p>\r\n\r\n<ul>\r\n	<li><strong>Waralaba Makanan</strong></li>\r\n</ul>\r\n\r\n<p>KFC | Jl. L.L.R.E. Martadinata No.72</p>\r\n\r\n<ul>\r\n	<li><strong>SPA</strong></li>\r\n</ul>\r\n\r\n<p>Atrium | Jl. Sukajadi No.168</p>\r\n\r\n<ul>\r\n	<li><strong>Hiburan Karaoke</strong></li>\r\n</ul>\r\n\r\n<p>Inul Vista Paskal | Paskal Hyper Square</p>\r\n\r\n<ul>\r\n	<li><strong>BPW Umum (Biro Perjalanan Wisata)</strong></li>\r\n</ul>\r\n\r\n<p>PT. Jackal Holidays | Jl. Soekarno-Hatta No.693</p>\r\n\r\n<ul>\r\n	<li><strong>BPW Haji &amp; Umrah (Biro Perjalanan Wisata)</strong></li>\r\n</ul>\r\n\r\n<p>Dago Wisata | Jl. Perintis Kemerdekaan No. 2</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Penghargaan Khusus Anugerah Pesona Pariwisata 2019 :</strong></p>\r\n\r\n<ul>\r\n	<li><strong>Pusat Perbelanjaan</strong></li>\r\n</ul>\r\n\r\n<p>23 Paskal | Jl. Pasir Kaliki No. 25-27</p>\r\n\r\n<ul>\r\n	<li><strong>Destinasi Umum</strong></li>\r\n</ul>\r\n\r\n<p>Saung Angklung Udjo | Jl. Padasuka No.118</p>\r\n', '2019-12-03 04:42:42', 2),
(16, 'Kemeriahan Glow Art Festival 2019', 'Kemeriahan_Glow_Art_Festival_2019.jpeg', '<p>Kemeriahan Glow Art Festival 2019 (GAF) telah selesai digelar di Taman Tegalega Bandung (26/09). Dibuka secara resmi oleh Wali Kota Bandung Oded M. Danial, penyelenggaraan GAF 2019 sekaligus menjadi penutup dari rangkaian acara Hari Jadi Kota Bandung ke-209.</p>\r\n\r\n<p>GAF 2019 menawarkan pengalaman baru bagi masyarakat untuk menikmati nuansa gemerlap keindahan Kota Bandung saat malam hari. Nuansa tersebut dihadirkan lewat banyaknya instalasi kendaraan hias, lampion, dan multimedia. Ada juga beragam atraksi seni dan budaya tradisi yang dikolaborasikan dengan kecanggihan teknologi masa kini. Di antaranya Tron Dance, Music Performance, Rampak Percussion, dan Projection Video Mapping.</p>\r\n\r\n<p>Antusiasme masyarakat pun semakin tinggi saat Glowing Parade dimulai. Glowing Parade menghadirkan parade kostum dan atribut hias berpendar dengan tema flora fauna khas Kota Bandung. Parade diikuti sebanyak 30 grup yang terdiri dari SKPD, kewilayahan, komunitas, serta perusahaan swasta yang ada di Kota Bandung. Tidak hanya itu saja, ada persembahan menarik yaitu pertunjukkan Wayang Kreasi oleh Opick Sunandar Sunarya yang dikolaborasikan dengan konten digital multimedia.</p>\r\n\r\n<p>Sebagai bentuk apresiasi, Disbudpar Kota Bandung memberikan sebuah penghargaan khusus kepada pihak yang terlibat. &nbsp;Berikut ini adalah daftar pemenang yang ikut serta dalam GAF 2019 :</p>\r\n\r\n<p><br />\r\n<strong>Juara 1</strong>&nbsp;:&nbsp;PDAM Tirtawening<br />\r\n<strong>Juara 2</strong>&nbsp;: Satpol PP Bandung<br />\r\n<strong>Juara 3</strong>&nbsp;:&nbsp;Kecamatan Astanaanyar<br />\r\n<br />\r\n<strong>Juara Kreasi 1</strong>&nbsp;:&nbsp;Dinsosnangkis<br />\r\n<strong>Juara Kreasi 2</strong>&nbsp;: BPKA<br />\r\n<strong>Juara Kreasi 3</strong>&nbsp;:&nbsp;Dinkes Kota Bandung<br />\r\n<br />\r\n<strong>Juara Harapan 1</strong>&nbsp;:&nbsp;Kecamatan Batununggal<br />\r\n<strong>Juara Harapan 2</strong>&nbsp;:&nbsp;Kecamatan Sukajadi<br />\r\n<strong>Juara Harapan 3</strong>&nbsp;:&nbsp;Diskominfo Bandung<br />\r\n<br />\r\n<strong>Juara Favorit 1</strong>&nbsp;:&nbsp;Disdik Kota Bandung<br />\r\n<strong>Juara Favorit 2</strong>&nbsp;: Damkar Kota Bandung<br />\r\n<strong>Juara Favorit 3</strong>&nbsp;:&nbsp;DPMTSP Kota Bandung</p>\r\n', '2019-12-03 04:44:27', 2),
(17, 'Final Pasanggiri Mojang Jajaka 2019', 'Final_Pasanggiri_Mojang_Jajaka_2019.jpeg', '<p>Sabtu (26/10), Dinas Kebudayaan dan Pariwisata Kota Bandung bersama Paguyuban Mojang Jajaka Kota Bandung menyelenggarakan Final Pasanggiri Mojang Jajaka Kota Bandung 2019. Bertempat di Hotel Harris Festival Citylink, Final Pasanggiri Mojang Jajaka 2019 dihadiri oleh Wakil Walikota Bandung Yana Mulyana, Miss Heritage Japan 2017 Nanami Chinatsu, perwakilan duta wisata beberapa kota/kabupaten di Indonesia, dan pendukung para finalis.</p>\r\n\r\n<p>Pada Pasanggiri Mojang Jajaka tahun ini, diterapkan metode baru yang lebih inovatif. Para finalis tidak lagi menjawab pertanyaan secara individual pada saat malam final melainkan memaparkan rancangan ide kreatif mereka secara berkelompok. Metode baru ini menantang para finalis untuk terjun langsung ke masyarakat kemudian membuat sebuah rancangan ide kreatif yang berangkat dari isu ataupun permasalahan yang mereka temui di lingkungan masyarakat kota Bandung.</p>\r\n\r\n<p>Berikut ide kreatif dari masing-masing kelompok:</p>\r\n\r\n<p>Kelompok 1: Pengenalan Objek Pemajuan Kebudayaan Melalui Web Series</p>\r\n\r\n<p>Kelompok 2: Wayang Golek dalam Digital Platform</p>\r\n\r\n<p>Kelompok 3: Teknik Lukis Tradisional Sunging</p>\r\n\r\n<p>Kelompok 4: Kuliner Tradisional Kadedemes</p>\r\n\r\n<p>Kelompok 5: Revitalisasi Keramik Kiaracondong</p>\r\n\r\n<p>Kelompok 6: Minuman Kesehatan Tradisional</p>\r\n\r\n<p>Dengan diterapkannya metode baru ini diharapkan ke 24 finalis dapat menciptakan solusi dan inovasi. Disbudpar Kota Bandung juga berharap kepada 24 finalis yang terpilih dapat merangsang perkembagan budaya yang mulai hilang dan menjadi contoh juga panutan bagi para pemuda/pemudi agar tetap menjaga warisan budaya.</p>\r\n\r\n<p>Hasil penjurian merupakan akumulasi dari seluruh rangkaian Pasanggiri Mojang Jajaka 2019 yang telah dilaksanakan selama kurang lebih sebulan terakhir. Adapun dewan juri untuk malam final yaitu&nbsp; Bapak Tjep Dahyat, Kevin Liliana (Miss International 2017), Kasep Ruenditama (Professional Photographer), Frisca Clarissa Almira (Putri Bunga 2011, News Presenter, Journalist Kompas TV), dan Riko Anggara (News Presenter &amp; Executive Producer Kompas TV).</p>\r\n\r\n<p>Berikut adalah tiga besar Mojang Jajaka Kota Bandung 2019:</p>\r\n\r\n<p>- Mojang Jajaka Pinilih Kota Bandung 2019: Farhana Nariswari &amp; Faturrohman Muhammad</p>\r\n\r\n<p>- Wakil 1/Runner Up Mojang Jajaka Kota Bandung 2019: First Trinna &amp; Singgih Eka</p>\r\n\r\n<p>- Wakil 2 Mojang Jajaka Kota Bandung 2019: Alma Zoraya &amp; Muhamad Sinki</p>\r\n\r\n<p>Selain itu berikut adalah pemenang dari kategori lainnya:</p>\r\n\r\n<p>- Mojang Jajaka Kameumeut 2019: Keisha Fathya &amp; Qeisha Muhammad</p>\r\n\r\n<p>- Mojang Jajaka Pinunjul Unjuk Kabisa 2019: Sharfina Annisa &amp; Galang Amarta</p>\r\n\r\n<p>- Mojang Jajaka Mimitra 2019: Flores Cantika &amp; Sultan Raihan</p>\r\n\r\n<p>Selamat kepada para pemenang dan finalis lainnya, semoga dapat mengemban tugas dan kewajibannya dengan baik, amanah, dan penuh tanggung jawab.</p>\r\n', '2019-12-03 04:45:54', 2);

-- --------------------------------------------------------

--
-- Table structure for table `detail_user`
--

CREATE TABLE `detail_user` (
  `id` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `foto_ktp` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `surat_izin_usaha` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_user`
--

INSERT INTO `detail_user` (`id`, `id_user`, `foto`, `foto_ktp`, `alamat`, `surat_izin_usaha`, `email`, `no_hp`) VALUES
(4, 23, 'Screenshot_(1)7.png', 'Screenshot_(1)8.png', 'test', 'Screenshot_(1)9.png', 'hello.derusta@gmail.com', '081223230946');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `nama`, `foto`, `deskripsi`) VALUES
(1, 'Ardan Indie 7 Showcase 2019 Sukses Menghibur Insan Muda', 'event.jpg', '<h3>EVENT, infobdg.com &ndash; Ada yang menarik dari acara Ardan Indie 7 Showcase 2019 yang digelar akhir pekan lalu di Bumi Sangkuriang. Tiga dari delapan musisi membawakan lagu dengan judul maupun lirik yang memiliki penggalan kata &ldquo;hujan&rdquo;.</h3>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mulai dari RisaDimasta dengan lagu Hujannya, Sal Priadi dengan lirik rintik-rintik pada lagu Amin Paling Serius, dan Lyon saat membawakan Asa &amp; Ruang. Tentu ini menjadi sebuah kebetulan yang pas dengan suasana hujan selama rangkaian acara berlangsung.</p>\r\n\r\n<p><a href=\"http://www.infobdg.com/v2/wp-content/uploads/2019/11/3-scaled.jpg\"><img alt=\"\" src=\"http://www.infobdg.com/v2/wp-content/uploads/2019/11/3-1024x576.jpg\" style=\"height:392px; width:696px\" /></a></p>\r\n\r\n<p>Dimulai sejak pukul tiga sore dengan diawali persembahan kabaret oleh Teater Bosmat Kabaret, lalu iringan&nbsp;<em>epic</em>&nbsp;instrumen angklung yang dibawakan Manshur Angklung seolah membawa pesan untuk tetap mengingat jasa dan perjuangan para pahlawan tetap harus disuarakan oleh insan muda sebagai generasi milenial yang melek musik, tanpa melupakan jati diri sebuah bangsa yang berhasil merdeka atas kontribusi luar biasa para Pahlawan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Belum lagi instrumen Angklung yang semakin menambah kecintaan terhadap instrumen musik lokal di era yang sudah modern ini. Betapa sebuah kolaborasi luar biasa dalam membuka Ardan Indie 7 Showcase 2019 yang digelar bertepatan dengan hari Pahlawan 10 November 2019.</p>\r\n\r\n<p>Walaupun harus berjibaku dengan rintik hujan, insan muda Ardan Radio nampaknya tetap dapat menikmati penampilan seluruh&nbsp;<em>line up</em>&nbsp;dengan mengenakan jas hujan yang direkomendasikan dibawa dan menyaksikan langsung dari &aacute;rea koridor ballroom Bumi Sangkuriang. Beberapa lagu dari&nbsp;<em>line up</em>&nbsp;pun menjadi sebuah momen menyanyi bersama untuk insan muda. Float, Pusakata, Lyon, Tashoora, Loner Lunar, Sal Priadi, RisaDimasta, juga Mawang memberikan sebuah kesatuan warna penampilan musik yang berbeda sesuai dengan karakternya masing-masing. Belum lagi viralnya Mawang dengan lagu andalannya kasih sayang kepada orang tua dan aku kamu, menjadi suguhan baru bagi insan muda penikmat musik indie Ardan Radio.</p>\r\n\r\n<p><a href=\"http://www.infobdg.com/v2/wp-content/uploads/2019/11/2.jpg\"><img alt=\"\" src=\"http://www.infobdg.com/v2/wp-content/uploads/2019/11/2-1024x1024.jpg\" style=\"height:696px; width:696px\" /></a></p>\r\n\r\n<p><em>&ldquo;</em>Pemilihan line up menjadi begitu penting untuk menyuguhkan rangkaian acara Ardan Indie 7 Showcase 2019,<em>&rdquo;&nbsp;</em>jelas Awan Yudha sebagai Music Director Ardan Radio.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Mengenai tema&nbsp;<em>wardrobe</em>&nbsp;yang dikenakan para penyiar Ardan Radio, untuk kedua kalinya Ardan mempercayakan Mussahendriks sebagai desainer&nbsp;<em>wardrobe</em>&nbsp;yang memproduksi&nbsp;<em>local brand</em>&nbsp;bernama Yoseph Kusumah,<em>&nbsp;streetwear</em>&nbsp;yang menonjolkan hasil karya&nbsp;<em>patchwork</em>&nbsp;atau potongan kain-kain perca yang menghasilkan motif dengan bentuk yang baru. Berkat&nbsp;<em>wardrobe</em>&nbsp;inilah&nbsp;<em>look</em>&nbsp;dari para penyiar Ardan terlihat berbeda dari siapapun yang hadir dalam rangkaian&nbsp;<em>event</em>&nbsp;Ardan Indie 7 Showcase 2019.</p>\r\n\r\n<p>Rangkaian acara Ardan Indie 7 Showcase 2019 telah digelar untuk kedua kalinya oleh Ardan Radio sebagai bagian dari&nbsp;<em>event</em>&nbsp;program siaran Ardan Indie 7, sebuah program&nbsp;<em>chart&nbsp;</em>atau tangga lagu Indie di Ardan Radio. Tahun 2019 menjadi tahun dengan momentum spesial dikarenakan diselenggarakan bertepatan dengan Hari Pahlawan.</p>\r\n\r\n<p><a href=\"http://www.infobdg.com/v2/wp-content/uploads/2019/11/1-scaled.jpg\"><img alt=\"\" src=\"http://www.infobdg.com/v2/wp-content/uploads/2019/11/1-1024x576.jpg\" style=\"height:392px; width:696px\" /></a></p>\r\n\r\n<p>&ldquo;Kami sangat berterimakasih untuk seluruh sponsor dan pihak-pihak yang telah mendukung jalannya acara ini, terutama untuk GG Signature Time Out, Drink Beng-beng, Bright, dan juga Link Aja. Tentu untuk seluruh media partner yang juga mendukung publikasi dan promosi rangkaian&nbsp;<em>event</em>, mulai dari Infobdg, Majalah GADIS, Wisata Bandung, Event Bandung, Warga Gigs, Starglam Magazine, What&rsquo;s New Indonesia,&rdquo; ucap Jan Peter Lucky Harianja selaku Program Manager Ardan Radio.</p>\r\n\r\n<p>Ardan Radio juga turut berterimakasih kepada&nbsp;<em>food partners</em>&nbsp;diantaranya Jumbo Eatery, Nasi Kulit Malam Minggu, BowlJug, dan Kandang Ayam, termasuk hotel&nbsp;<em>partners</em>&nbsp;diantaranya Bel Viu Hotel, Grand Mercure Setiabudi, dan Ardan Hotel. Sampai jumpa kembali di rangkaian acara ARDAN Radio selanjutnya.&nbsp;<em>Stay Cool and Lovely!!!</em></p>\r\n'),
(2, 'Industry 4.0 Edufair', 'event1.jpg', '<p>Waktu:<br />\r\n21 November 2019</p>\r\n\r\n<p>Tempat:<br />\r\nNovotel Hotel, Jl Cihampelas no 23-25, Bandung</p>\r\n\r\n<p>Informasi Selengkapnya:</p>\r\n\r\n<p><a href=\"https://twitter.com/vistaeducation\">@vistaeducation</a></p>\r\n\r\n<p><a href=\"http://instagram.com/vistaeducation\">@vistaeducation</a></p>\r\n\r\n<p><a href=\"http://vistaeducation.com/\">vistaeducation.com</a></p>\r\n'),
(3, 'Celebration of Christmas', 'event.jpeg', '<h1>Celebration of Christmas</h1>\r\n\r\n<p>Waktu:</p>\r\n\r\n<p>2 November 2019</p>\r\n\r\n<p>Wings Cup 2019 &ldquo;Best of Best&rdquo;</p>\r\n\r\n<p>3 November 2019</p>\r\n\r\n<p>Braga Showcase with KPH Music</p>\r\n\r\n<p>10 November 2019</p>\r\n\r\n<p>Braga Cat Show 2019</p>\r\n\r\n<p>21 November 2019</p>\r\n\r\n<p>Braga Jazz Night</p>\r\n\r\n<p>Setiap Senin : Easy Monday (Candle Light Piano Playing)<br />\r\nSetiap Rabu : Kacapi Suling Braga (Traditional Music Playing)<br />\r\nSetiap Jumat : TGIF! (Live Band Performance)</p>\r\n\r\n<p>Tempat:<br />\r\nBraga Citywalk</p>\r\n\r\n<p>Informasi Selengkapnya:</p>\r\n\r\n<p><a href=\"https://twitter.com/braga_citywalk\">@braga_citywalk</a></p>\r\n\r\n<p><a href=\"https://instagram.com/bragacitywalk\">bragacitywalk</a></p>\r\n\r\n<p><a href=\"https://facebook.com/bragacitywalk\">braga citywalk</a></p>\r\n\r\n<p><a href=\"https://bragacitywalk.co.id/\">bragacitywalk.co.id</a></p>\r\n'),
(4, 'Dari Kick Fest, Sampai West Java Festival Ada di #BandungEventThisWeek Minggu Ini', 'event1.jpeg', '<p>EVENT, infobdg.com &ndash; Acara di Bandung minggu ini banyak yang menarik nih Wargi Bandung. Penasaran ada acara apa saja? Cek yuk di #BandungEventThisWeek.</p>\r\n\r\n<p>1Hijab Market Season Sale 2</p>\r\n\r\n<p>Wargi Bandung lagi cari perlengkapan hijab? Datang aja ke acara ini nih. Di sini ada banyak&nbsp;booth&nbsp;hijab yang bisa mojang Bandung pilih. Selain itu ada juga&nbsp;inspiring talk show, dan masih banyak lagi acara seru lainnya. Acara ini diadakan di Sabuga dari tanggal 30-31 Oktober 2019.</p>\r\n\r\n<p>2Simpati Kickfest XIII</p>\r\n\r\n<p>Awal bulan ada rencana belanja? Wargi Bandung bisa banget datang ke acara ini. Akan ada banyak&nbsp;brand clothing&nbsp;lokal yang hadir di Kickfest kali ini. Selain itu akan ada pasar komunitas,&nbsp;extreme sport,&nbsp;creative area,&nbsp;culinary,&nbsp;shopping program, dan ruang ide.&nbsp;Tak ketinggalan akan ada juga penampilan dari Ardhito Pramono, Efek Rumah Kaca, Fourtwnty, Feel Koplo, Koil, Revenge The Fate, dan masih banyak lagi. Acara ini diadakan pada tanggal 31 Oktober-3 November 2019 di PPI Pusenif.</p>\r\n\r\n<p>3West Java Festival 2019</p>\r\n\r\n<p>Dalam rangka memperingati HUT Provinsi Jawa Barat ke-74, Dinas Pariwisata Provinsi Jawa Barat mengadakan West Java Festival 2019. Acara yang diselenggarakan di Gedung Sate pada tanggal 1-3 November ini akan menampilkan Gigi, Dewi Gita, Bara Suara, Isyana Sarasvati, Yura Yunitam Andi /RIF, dan masih banyak lagi.</p>\r\n\r\n<p>4Anniversaeed 2019</p>\r\n\r\n<p>Setelah lama tidak manggung di Bandung, Fiersa Besari akhirnya manggung lagi di acara Anniversaeed 2019. Acara ini merupakan acara tahunan dari HMP-S PBI UIN Sunan Gunung Djati. Selain penampilan dari Fiersa Besari, akan ada juga donor darah, dan bazzar. Anniversaeed 2019 diselenggarakan di IFI Bandung pada tanggal 2 November 2019.</p>\r\n\r\n<p>5The 16th Career Days Of Telkom University</p>\r\n\r\n<p>Buat Wargi Bandung yang masih cari kerja, minggu ini ada lagi nih&nbsp;job fair&nbsp;yang diselenggarakan oleh Telkom University.&nbsp;Job fair&nbsp;ini akan menghadirkan puluhan perusahaan, dan akan juga menghadirkan&nbsp;scholarship sharing session&nbsp;dari LPDP. Acara ini diselenggarakan di Telkom University pada tanggal 30-31 Oktober 2019.</p>\r\n\r\n<p>6Urflavor Market Chapter 16</p>\r\n\r\n<p>Wargi Bandung mau wisata kuliner? Minggu ini ada acara ini nih. Bakalan ada banyak&nbsp;tenant&nbsp;kuliner yang hadir di acara ini. Acara ini diselenggarakan di Plaza Area Trans Studio Mall, tanggal 30 Oktober &ndash; 3 November 2019</p>\r\n'),
(5, 'Band Noah Akan Kembali Menghentak Kota Bandung', 'event2.jpeg', '<h3>BANDUNG, infobdg.com &ndash; Band papan atas asal Kota Bandung, Noah, akhirnya tampil kembali di kota kelahirannya. Noah yang telah lama tidak melakukan konser di Kota Bandung akan menjadi&nbsp;<em>headline</em>&nbsp;dalam festival musik Now Playing Festival.</h3>\r\n\r\n<p><img alt=\"\" src=\"http://www.infobdg.com/v2/wp-content/uploads/2019/11/WhatsApp-Image-2019-11-23-at-14.33.05.jpeg\" style=\"height:853px; width:1280px\" /></p>\r\n\r\n<p>Sumber Foto: Istimewa</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Semenjak Noah merilis album &ldquo;Keterkaitan Keterikatan&rdquo; pada tanggal 14 Agustus 2019 kemarin, Noah juga dikabarkan akan segera melangsungkan konser tur solo di beberapa Kota di Indonesia pada tahun 2020 nanti.</p>\r\n\r\n<p>Media Officer Now Playing Festival, Aris Munandar mengatakan, Noah akan mengisi Now Playing Festival yang rencananya dilangsungkan pada tanggal 22 Desember 2019 mendatang. &ldquo;Iya, Noah sudah masuk<em>&nbsp;line up</em>&nbsp;pengisi acara nanti, kan udah lama juga gak main di Bandung,&rdquo; kata Aris di Bandung, Sabtu (23/11).</p>\r\n\r\n<p>Menurut Aris, Meskipun albumnya telah diterima dengan baik oleh seluruh&nbsp;<em>fans</em>&nbsp;Noah di seluruh Kota di Indonesia, namun Kota Bandung sepertinya tidak akan menjadi kota yang disambangi dalam tur album baru Noah. &ldquo;Tapi di acara kami nanti, Sahabat Noah Kota Bandung dan sekitarnya bisa lihat Ariel dan kawan kawan langsung, ya ini angin segar lah buat sahabat Noah,&rdquo; tuturnya.</p>\r\n\r\n<p>Antusiasme Sahabat Noah Bandung juga sudah terlihat, Aris mengklaim, tiket Now Playing Festival ini hanya dalam waktu kurang dari satu jam langsung diserbu setelah Noah dikonfirmasikan akan tampil di acara tersebut.</p>\r\n\r\n<p>Festival Musik yang akan dilaksanakan pada tanggal 22 desember 2019 di Lapangan Pussenif Kota Bandung ini juga akan menampilkan banyak nama besar lain seperti Naif, Tulus, Raisa, Fourtwnty, dsb.</p>\r\n'),
(6, 'Ramadhan Festival Citylink', 'festival_citylink.jpg', '<p><strong>Ramadhan&nbsp;Festival Citylink<br />\r\nTanggal</strong><strong>&nbsp;:</strong>&nbsp;4 Mei &ndash; 8 Juni 2018<br />\r\n<strong>Tempat</strong><strong>&nbsp;:</strong>&nbsp;Festival Citylink Mall Bandung</p>\r\n\r\n<p><strong>Deskrispi Event :</strong><br />\r\nSelamat menjalani ibadah puasa Shopperfest, dibulan suci Ramadhan ini Festival Citylink banyak menyuguhkan event yang seru-seru dan banyak hadiah juga untuk kamu. Stay tune&nbsp;@festivalcitylink!</p>\r\n\r\n<p>Catat tanggal &amp; waktunya karena Late Night Shopping akan diadakan tanggal 25 Mei 2019 dengan menghadirkan guest star Hedi Yunus! Yuk belanja sambil seru-seruan menyanyikan lagu nostalgia, yaaash!</p>\r\n\r\n<p>Segera daftarkan diri kamu menjadi member Festive Card karena ada banyak keuntungan dan hadiah yang bisa kamu dapat dengan mengumpulkan poin sebanyak-banyaknya.</p>\r\n\r\n<p><strong>More Information :</strong></p>\r\n\r\n<ul>\r\n	<li>Hotline : (022) 6128585</li>\r\n</ul>\r\n'),
(7, ' Glow Art Festival 2019', 'Glow_Art_Festival_2019.jpeg', '<p><strong>PRESS RELEASE</strong></p>\r\n\r\n<p><strong>Glow Art Festival 2019 Siap Ramaikan Kota Bandung</strong></p>\r\n\r\n<p>Pemerintah Kota Bandung melalui Dinas Kebudayaan dan Pariwisata (Disbudpar) akan menggelar acara Glow Art Festival (GAF) 2019 pada tanggal 26 Oktober di Taman Tegalega. Gelaran ini merupakan puncak peringatan Hari Jadi Kota Bandung (HJKB) ke-209. Mengusung format Neon Carnival, penyelenggaraan GAF 2019 akan memamerkan dua ikon flora dan fauna Kota Bandung yaitu Bunga Patrakomala dan Burung Cangkurileung/Kutilang.</p>\r\n\r\n<p>GAF 2019 menawarkan pengalaman baru bagi masyarakat untuk menikmati nuansa gemerlap keindahan Kota Bandung saat malam hari. Nuansa tersebut akan dihadirkan lewat banyaknya instalasi lampion, lighting, dan multimedia. Tidak hanya itu saja, akan ada juga beragam atraksi seni dan budaya tradisi yang dikolaborasikan dengan kecanggihan teknologi masa kini.</p>\r\n\r\n<p>Keseruan rangkaian acara GAF 2019 meliputi :</p>\r\n\r\n<p><strong>1. Glowing Carnival</strong></p>\r\n\r\n<p>Karnival pawai (kendaraan hias, kostum hias, dan komunitas dengan atribut flora dan fauna Kota Bandung.</p>\r\n\r\n<p><strong>2. Korean &amp; Japan Community Performance</strong></p>\r\n\r\n<p>Mempersembahkan dance cover dari Komunitas Tari Korea dan Jepang.</p>\r\n\r\n<p><strong>3. Tron Dance &amp; Traditional Performance</strong></p>\r\n\r\n<p>Membawa konsep futuristic, rangkaian acara ini menampilkan kesenian tradisi Sunda maupun tarian modern yang dilengkapi dengan glowing costume serta penggunaan unsur teknologi.</p>\r\n\r\n<p><strong>4. Culinary Festival</strong></p>\r\n\r\n<p>Ada berbagai jajanan makanan dan minuman yang bisa ditemukan dalam GAF 2019.</p>\r\n\r\n<p><strong>5. Wayang Fusion</strong></p>\r\n\r\n<p>Pagelaran Wayang yang dikemas dengan multimedia dan unsur digital untuk memberikan edukasi kepada generasi muda Kota Bandung.</p>\r\n\r\n<p><strong>6. Rampak Percussion Performance</strong></p>\r\n\r\n<p>Penampilan rampak kendang dan perkusi dengan paduan visual bercahaya.</p>\r\n\r\n<p>Catat tanggalnya dan jadi bagian dari kemeriahan Glow Art Festival 2019! Informasi selengkapnya silahkan kunjungi&nbsp;<a href=\"https://disbudpar.bandung.go.id/\">www.disbudpar.bandung.go.id</a>&nbsp;dan Instagram @disbudpar.bdg.</p>\r\n'),
(8, 'Investment, Tourism & Trade Expo (ITTEX) 2019', 'greentex-PPN-ITEEX-Kopi-Coklat-Meri-04-04.jpg', '<p><strong>Investment, Tourism &amp; Trade Expo (ITTEX) 2019</strong></p>\r\n\r\n<p><strong>Tanggal:&nbsp;</strong>22-25 Agustus 2019<br />\r\n<strong>Tempat:</strong>&nbsp;Manggala Siliwangi Bandung, Jl. Aceh No.66, Merdeka, Kec. Sumur Bandung, Kota Bandung, Jawa Barat 40113&nbsp;<br />\r\n<strong>Jam Buka</strong>&nbsp;Pameran Weekday &amp; Weekend: 10.00 &ndash; 21.00 WIB<br />\r\n<strong>HTM:</strong>&nbsp;GRATIS</p>\r\n'),
(9, 'Travel Mart 2019 Sekolah Tinggi Pariwisata NHI Bandung', 'Poster-Travel-Mart-2019-potrait-spons.jpg', '<p>Mahasiswa Program Studi Manajemen Bisnis Perjalanan Sekolah Tinggi Pariwisata NHI Bandung yang berada di Setiabudhi 186, tahun ini kembali akan menggelar sebuah acara tahunan yang bertajuk &ldquo;Travel Mart 2019&rdquo;. Kegiatan ini merupakan sebuah travel trade exhibition-featuring networking and contrading opportunities yang bertujuan untuk membantu para pelaku bisnis perjalanan serta bisnis pariwisata yang menjadi tempat dalam mengambil keputusan memilih produk wisata, mempertemukan klien baru, memperluas jaringan para sellers maupun buyers dan terjalinnya hubungan kerja baru ataupun memperkuat bisnis yang sudah pernah terjalin sebelumnya.</p>\r\n\r\n<p>Travel Mart ke 22 ini mengusung tema &ldquo;The Authenticity of Indonesia&rdquo;, konsep Travel Mart tahun ini yaitu mempromosikan keunikan serta kekhasan Objek Daya Tarik Wisata (ODTW) di Indonesia. Bertempat di Dome Malabar STP NHI Bandung, acara ini akan diselenggarakan pada tanggal 24 s.d 25 April 2019. Diselenggarakan selama 2 hari dan terbagi menjadi dua program utama yaitu hari pertama dikemas dalam bentuk system B2B (Business To Business) dimana mempertemukan para seller dan buyer secara langsung untuk bertukar informasi mengenai produk yang dimiliki. Pertemuan tersebut diatur dengan system Round Robin Table Top dengan tujuan menciptakan komunikasi serta hubungan kerja bisnis yang efektif dengan mempertemukan kurang lebih 36 Domestic sellers yang terdiri dari Airline, Hotel, Atraksi Wisata dan pelaku industry perjalanan dari 17 provinsi di Indonesia (Sumatra Utara, Bangka Belitung, Lampung, Aceh, Jawa Barat, Jawa Tengah, Jawa Timur, D.I Yogyakarta, Banten, DKI Jakarta, Bali, Nusa Tenggara Barat, Nusa Tenggara Timur, Kalimantan Tengah, Kalimantan Timur, Sulawesi Tenggara dan Maluku) dengan 66 Domestic Corporate Buyers (yang didatangi dari Provinsi Jawa Barat, DKI Jakarta, Jawa Tengah, Jawa Timur, Sumatera, Nusa Tenggara Barat, Nusa Tenggara Timur) dan 7 International Corporate Buyers (Singapore, Vietnam, Malaysia, Thailand, Filipina, Australia). Sedangkan pada hari kedua acara ini terbuka untuk masyarakat umum (Public Hour) atau disebut B2C (Business To Customer). Pada kesempatan ini pengunjung akan mendapatkan informasi seputar produk perjalanan wisata yang mereka butuhkan.</p>\r\n\r\n<p>Selain acara utama, pada hari kedua terdapat berbagai rangkaian acara menarik seperti talkshow yang akan disampaikan oleh Direktur Digital Frontier Group, Owner Travel Whatravel Travel Blogger, serta akan dipandu oleh Moderator dari STP NHI Bandung. Pengunjung juga menikmati beberapa sajian hiburan berupa tarian tradisional, pertunjukan musik Saung Angklung Udjo saat Opening Ceremony dan booth kuliner dari berbagai daerah serta beragam hadiah menarik dari berbagai daerah di Indonesia yang pastinya sayang untuk dilewatkan.</p>\r\n'),
(10, 'PAMERAN BANDUNG EXPLORE WISATA INDONESIA 2019', 'bandung-explore.jpg', '<p><strong>PAMERAN BANDUNG EXPLORE WISATA INDONESIA 2019</strong></p>\r\n\r\n<p><strong>TANGGAL:</strong>&nbsp;26 &ndash; 28 April 2019<br />\r\n<strong>TEMPAT:&nbsp;</strong>Bandung<br />\r\n<strong>HTM:</strong>&nbsp;GRATIS!</p>\r\n\r\n<p><strong>TEMA:</strong>&nbsp;&rdquo; Eco Tourism, Berwisata sambil menjaga lingkungan &rdquo;</p>\r\n\r\n<p><strong>MAKSUD &amp; TUJUAN :</strong></p>\r\n\r\n<p>Menggali destinasi wisata baru yang potensial dari berbagai daerah di Indonesia dan untuk memperkenalkan dan mempromosikan obyek &ndash; obyek wisata unggulan disetiap Provinsi, Kabupaten, Kota, kepada wisatawan domestik dan mancanegara agar dapat menambah Pendapatan Asli Daerah (PAD) serta memberi peluang kepada investor agar dapat mengembangkan usaha disektor pendukung pariwisata, seperti : Perhotelan, transportasi, Rumah Makan dan Fasilitas lainnya.</p>\r\n\r\n<p><strong>INFO DAN KONTAK:</strong></p>\r\n\r\n<p>PT. Yuka Mitra Promo (YUKAPRO)<br />\r\nEmail : info.yukapro@gmail.com<br />\r\nTwitter : @27yukamp<br />\r\nFacebook : Yukapro<br />\r\nInstagram : Yukapro</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama_kategori` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama_kategori`) VALUES
(1, 'Wisata Outdoor'),
(2, 'Wisata Indoor'),
(3, 'Wisata Kuliner'),
(4, 'Wisata Religi'),
(5, 'Wisata Budaya');

-- --------------------------------------------------------

--
-- Table structure for table `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `level`
--

INSERT INTO `level` (`id`, `nama`) VALUES
(1, 'admin'),
(2, 'pemilik wisata'),
(3, 'wisatawan');

-- --------------------------------------------------------

--
-- Table structure for table `penginapan`
--

CREATE TABLE `penginapan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `kategori` tinyint(4) NOT NULL,
  `waktu_upload` datetime NOT NULL,
  `google_maps` text NOT NULL,
  `harga` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statistik_pengunjung`
--

CREATE TABLE `statistik_pengunjung` (
  `id` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `hits` int(10) NOT NULL,
  `online` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `statistik_pengunjung`
--

INSERT INTO `statistik_pengunjung` (`id`, `ip`, `tanggal`, `hits`, `online`) VALUES
(1, '::1', '2020-01-01', 3, '1579671676'),
(2, '::1', '2020-01-02', 10, '1579671703'),
(3, '::1', '2020-01-03', 17, '1579671738'),
(4, '::1', '2020-01-04', 43, '1579671776'),
(5, '::1', '2020-01-05', 10, '1579671801'),
(6, '::1', '2020-01-06', 1, '1579671816'),
(7, '::1', '2020-01-07', 6, '1579671882'),
(8, '::1', '2020-01-08', 16, '1579671901'),
(9, '::1', '2020-01-09', 3, '1579671916'),
(10, '::1', '2020-01-10', 3, '1579671927'),
(11, '::1', '2020-01-11', 33, '1579671983'),
(12, '::1', '2020-01-12', 5, '1579672004'),
(13, '::1', '2020-01-13', 15, '1579672043'),
(14, '::1', '2020-01-14', 9, '1579672057'),
(15, '::1', '2020-01-15', 5, '1579672069'),
(16, '::1', '2020-01-16', 9, '1579672082'),
(17, '::1', '2020-01-17', 17, '1579672099'),
(18, '::1', '2020-01-18', 20, '1579672116'),
(19, '::1', '2020-01-19', 47, '1579672194'),
(20, '::1', '2020-01-20', 23, '1579672209'),
(21, '::1', '2020-01-21', 16, '1579672226'),
(22, '::1', '2020-01-22', 88, '1579679235');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `id_level` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `id_level`, `nama_lengkap`, `username`, `password`, `status`) VALUES
(22, 1, 'Admin Pariwisata', 'admin', '123456', 1),
(23, 2, 'dede rusliandi', 'dede', '123456', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wisata`
--

CREATE TABLE `wisata` (
  `id` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `foto_2` varchar(255) DEFAULT NULL,
  `foto_3` varchar(255) DEFAULT NULL,
  `foto_4` varchar(255) DEFAULT NULL,
  `foto_5` varchar(255) DEFAULT NULL,
  `foto_6` varchar(255) DEFAULT NULL,
  `foto_7` varchar(255) DEFAULT NULL,
  `foto_8` varchar(255) DEFAULT NULL,
  `foto_9` varchar(255) DEFAULT NULL,
  `foto_10` varchar(255) DEFAULT NULL,
  `surat_izin_usaha` varchar(255) NOT NULL,
  `link_youtube` varchar(255) DEFAULT NULL,
  `id_youtube` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga_tiket` double NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `waktu_upload` datetime NOT NULL,
  `google_maps` text NOT NULL,
  `alasan` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `detail_user`
--
ALTER TABLE `detail_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `detail_user_id_user_foreign` (`id_user`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penginapan`
--
ALTER TABLE `penginapan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statistik_pengunjung`
--
ALTER TABLE `statistik_pengunjung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_level_foreign` (`id_level`);

--
-- Indexes for table `wisata`
--
ALTER TABLE `wisata`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wisata_id_kategori_foreign` (`id_kategori`),
  ADD KEY `wisata_id_user_foreign` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `detail_user`
--
ALTER TABLE `detail_user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `penginapan`
--
ALTER TABLE `penginapan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `statistik_pengunjung`
--
ALTER TABLE `statistik_pengunjung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `wisata`
--
ALTER TABLE `wisata`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `detail_user`
--
ALTER TABLE `detail_user`
  ADD CONSTRAINT `detail_user_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_id_level_foreign` FOREIGN KEY (`id_level`) REFERENCES `level` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `wisata`
--
ALTER TABLE `wisata`
  ADD CONSTRAINT `wisata_id_kategori_foreign` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `wisata_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
