<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if(!$this->session->userdata("username")) redirect("login");
        $this->load->model('Event_model');
	}
	
	public function index()
	{
        $data['dataEvent'] = $this->Event_model->read("","id DESC");
        $data['view'] = 'admin/event/index';
        $this->load->view('admin', $data);
    }
    
    public function create()
    {
        $data['view'] = 'admin/event/create';
        $this->load->view('admin', $data);
    }

    public function store() 
    {  
        if(!$this->input->post('foto')) {
			$config['upload_path']		= './uploads/event';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto')) {
                echo "errors";
			}
			else
			{
				$upload_data = $this->upload->data();
				$foto = $upload_data['file_name'];
			}
		}

		$data	= array(
			'nama' => $this->input->post("nama"),
			'foto' => $foto,
			'deskripsi'	=> $this->input->post("deskripsi"),
		);

		$this->Event_model->create($data);

		redirect("auth/event");
    }

    public function edit($id)
	{
        $data['view'] = 'admin/event/edit';
		$data['event'] = $this->Event_model->read("event.id = '$id'")[0];
		$this->load->view('admin', $data);
    }
    
    public function show($id)
    {
        $data['event'] = $this->Event_model->read("event.id = '$id'")[0];
        $data['view'] = 'admin/event/show';
		$this->load->view('admin', $data);
    }

	public function update($id)
	{
		$config['upload_path'] = './uploads/event/';
        $config['allowed_types'] = 'gif|jpeg|jpg|png';

        $this->load->library('upload', $config);
		if (!$this->upload->do_upload('foto')){
            echo "errors";
        }
        else
		{
			$this->db->select("id,foto");
	        $this->db->from("event");
	        $this->db->where("id = '$id'");
	        $query = $this->db->get();

	        if($query->num_rows() > 0) {
	            foreach($query->result() as $r) {
					$foto		= $r->foto;
	        	}
	            $dir = './uploads/event/';
	            opendir($dir);
	            unlink($dir.$foto);
	        }

	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto'	=> $upload_content['file_name'],
	            );
	        $this->Event_model->update("id = '$id'",$data);
        }

		$data	= array(
			'nama' => $this->input->post("nama"),
			'deskripsi'	=> $this->input->post("deskripsi"),
        );

		$this->Event_model->update("id = '$id'",$data);

        redirect("auth/event");
    }
    
    public function delete($id)
    {
        $this->db->select("id,foto");
        $this->db->from("event");
        $this->db->where("id = '$id'");
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            foreach($query->result() as $r) {
                $foto		= $r->foto;
            }
            $dir = './uploads/event/';
            opendir($dir);
            unlink($dir.$foto);
        }

        $this->Event_model->delete("event.id = '$id'");

        redirect("auth/event");
    }
}
