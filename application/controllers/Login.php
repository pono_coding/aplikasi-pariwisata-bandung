<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
		parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
		
        $this->load->model("Login_model");
        $this->load->model("Detail_user_model");
    }

    function index()
    {
        $this->load->view("admin/login/index");
    }

    function do_login()
    {
        //ambil data dari v_login
		$username		= $this->input->post("username");
		$password		= $this->input->post("password");

		//ambil data berdasarkan where
		$where	= array(
			"username"		=> $username,
			"password"		=> $password,
		);
		$this->load->model("Login_model");
		$result	= $this->Login_model->read($where);

		if(count($result) != 0) {
			$this->session->set_userdata("username",$username);
			$id_user	= $result[0]->id;
			$this->session->set_userdata("id_user",$id_user);

			$id_level	= $result[0]->id_level;
			$this->session->set_userdata("id_level",$id_level);

			$status	= $result[0]->status;
			$this->session->set_userdata("status",$status);

			redirect("auth/dashboard");

		} else {
			//feedback jika gagal
			echo "<script>
			alert('Login gagal');
			window.location.href='".site_url('auth/login')."';
			</script>";
		}
	}
	
	function registrasi()
	{
        $this->load->view("admin/login/register");
	}

	function do_registrasi()
	{
		
		$username = $this->input->post("username");
		$password = $this->input->post("password");
		$nama_lengkap = $this->input->post("nama_lengkap");
		$id_level = $this->input->post("id_level");

		$username	= array(
			"username"		=> $username
		);
		$resultUsername	= $this->Login_model->read($username);

		if($password != $this->input->post('konfirmasi_password')) {
			echo "<script>
			alert('Konfirmasi Password Salah');
			window.location.href='".site_url('auth/registrasi')."';
			</script>";
		} elseif(count($resultUsername) != 0) {
			echo "<script>
			alert('Username Sudah ada');
			window.location.href='".site_url('auth/registrasi')."';
			</script>";
		}
		else {
			$this->Login_model->create(
				array(
					'username' => $this->input->post("username"),
					'password' => $password,
					'id_level' => $id_level,
					'nama_lengkap' => $nama_lengkap,
				)
			);
	
			echo "<script>
			alert('registrasi anda berhasil, silahkan login dengan akun yang ada.');
			window.location.href='".site_url('auth/login')."';
			</script>";
		}
	}

    function logout()
	{
		$this->session->sess_destroy();
		redirect("home");
	}
}
?>