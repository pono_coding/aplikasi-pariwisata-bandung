<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if(!$this->session->userdata("username")) redirect("login");
        $this->load->model('User_model');
        $this->load->model('Detail_user_model');
	}
	
	public function index()
	{
        $data['dataUser'] = $this->User_model->read("id_level = 2","id DESC");
        $data['view'] = 'admin/user/index';
        $this->load->view('admin', $data);
    }
    
    
    public function delete($id)
    {
        $this->User_model->delete("id = '$id'");
        redirect('auth/user');
    }

    public function acc($id) 
    {
        $this->User_model->update("id = '$id'", array("status" => 1));
        redirect('auth/user');
    }

    public function show($id)
    {
        $data['dataUser'] = $this->User_model->read("id = '$id'")[0];
        $data['detail_user'] = $this->Detail_user_model->read("id_user = '$id'")[0];
        $data['view'] = 'admin/user/show';
        $this->load->view('admin', $data);
    }
        
}
