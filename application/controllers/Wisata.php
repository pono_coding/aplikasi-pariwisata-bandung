<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class wisata extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if(!$this->session->userdata("username")) redirect("login");
        $this->load->model('Wisata_model');
        $this->load->model('Kategori_model');
        $this->load->model('Detail_user_model');
	}
	
	public function index()
	{
        if($this->session->userdata("id_level") == 1) {
            $data['dataWisata'] = $this->Wisata_model->read("","wisata.id DESC");
        } else {
            $id_user = $this->session->userdata("id_user");
            $data['dataWisata'] = $this->Wisata_model->read("wisata.id_user = '$id_user'","wisata.id DESC");
        }
        $data['view'] = 'admin/wisata/index';
        $this->load->view('admin', $data);
    }
    
    public function create()
    {
        if($this->session->userdata('id_level') == "2") {
            $id_user = $this->session->userdata("id_user");
			$checkData = $this->Detail_user_model->read("id_user = '$id_user'");

			if(empty($checkData)) {
				echo "<script>
                alert('Data Anda Tidak Lengkap. Silahkan Lengkapi Terlebih Dahulu');
                window.location.href='".site_url('auth/profil')."';
                </script>";
			}
			
            if($this->session->userdata("status") == 0) {
                echo "<script>
                alert('Anda tidak bisa mengakses tambah wisata. karena belum di ACC oleh admin');
                window.location.href='".site_url('auth/wisata')."';
                </script>";
            }
        }
        $data['dataKategori'] = $this->Kategori_model->read();
        $data['view'] = 'admin/wisata/create';
        $this->load->view('admin', $data);
    }

    public function store() 
    {  
        if(!$this->input->post('foto')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto')) {
                echo "errors";
			}
			else
			{
				$upload_foto = $this->upload->data();
				$foto = $upload_foto['file_name'];
			}
        }

        if(!$this->input->post('surat_izin_usaha')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('surat_izin_usaha')) {
                echo "errors";
			}
			else
			{
				$upload_surat_izin_usaha = $this->upload->data();
				$surat_izin_usaha = $upload_surat_izin_usaha['file_name'];
			}
        }
        
        if(!$this->input->post('foto_2')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_2')) {
                echo "errors";
			}
			else
			{
				$upload_foto_2 = $this->upload->data();
				$foto_2 = $upload_foto_2['file_name'];
			}
        }
        
        if(!$this->input->post('foto_3')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_3')) {
                echo "errors";
			}
			else
			{
				$upload_foto_3 = $this->upload->data();
				$foto_3 = $upload_foto_3['file_name'];
			}
        }
        
        if(!$this->input->post('foto_4')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_4')) {
                echo "errors";
			}
			else
			{
				$upload_foto_4 = $this->upload->data();
				$foto_4 = $upload_foto_4['file_name'];
			}
        }
        
        if(!$this->input->post('foto_5')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_5')) {
                echo "errors";
			}
			else
			{
				$upload_foto_5 = $this->upload->data();
				$foto_5 = $upload_foto_5['file_name'];
			}
        }
        
        if(!$this->input->post('foto_6')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_6')) {
                echo "errors";
			}
			else
			{
				$upload_foto_6 = $this->upload->data();
				$foto_6 = $upload_foto_6['file_name'];
			}
        }
        
        if(!$this->input->post('foto_7')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_7')) {
                echo "errors";
			}
			else
			{
				$upload_foto_7 = $this->upload->data();
				$foto_7 = $upload_foto_7['file_name'];
			}
        }
        
        if(!$this->input->post('foto_8')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_8')) {
                echo "errors";
			}
			else
			{
				$upload_foto_8 = $this->upload->data();
				$foto_8 = $upload_foto_8['file_name'];
			}
        }
        
        if(!$this->input->post('foto_9')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_9')) {
                echo "errors";
			}
			else
			{
				$upload_foto_9 = $this->upload->data();
				$foto_9 = $upload_foto_9['file_name'];
			}
        }
        
        if(!$this->input->post('foto_10')) {
			$config['upload_path']		= './uploads/wisata';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_10')) {
                echo "errors";
			}
			else
			{
				$upload_foto_10 = $this->upload->data();
				$foto_10 = $upload_foto_10['file_name'];
			}
		}

		$data	= array(
			'foto' => $foto,
			'surat_izin_usaha' => $surat_izin_usaha,
			'foto_2' => empty($foto_2) ? NULL : $foto_2,
			'foto_3' => empty($foto_3) ? NULL : $foto_3,
			'foto_4' => empty($foto_4) ? NULL : $foto_4,
			'foto_5' => empty($foto_5) ? NULL : $foto_5,
			'foto_6' => empty($foto_6) ? NULL : $foto_6,
			'foto_7' => empty($foto_7) ? NULL : $foto_7,
			'foto_8' => empty($foto_8) ? NULL : $foto_8,
			'foto_9' => empty($foto_9) ? NULL : $foto_9,
			'foto_10' => empty($foto_10) ? NULL : $foto_10,
			'nama' => $this->input->post("nama"),
			'id_kategori' => $this->input->post("id_kategori"),
            'status' => $this->input->post("status"),
            'id_user' => $this->session->userdata("id_user"),
            'deskripsi'	=> $this->input->post("deskripsi"),
            'harga_tiket'	=> $this->input->post("harga_tiket"),
            'waktu_upload' => date('y-m-d h:i:s'),
            'google_maps' => $this->input->post("google_maps"),
            'link_youtube' => $this->input->post("link_youtube"),
            'id_youtube' => $this->link_youtube($this->input->post("link_youtube")),
            'alasan' => $this->input->post("alasan"),
		);

		$this->Wisata_model->create($data);

		redirect("auth/wisata");
    }

    private function link_youtube($url) 
	{
	    $pattern = 
	        '%^# Match any youtube URL
	        (?:https?://)?  # Optional scheme. Either http or https
	        (?:www\.)?      # Optional www subdomain
	        (?:             # Group host alternatives
	          youtu\.be/    # Either youtu.be,
	        | youtube\.com  # or youtube.com
	          (?:           # Group path alternatives
	            /embed/     # Either /embed/
	          | /v/         # or /v/
	          | /watch\?v=  # or /watch\?v=
	          )             # End path alternatives.
	        )               # End host alternatives.
	        ([\w-]{10,12})  # Allow 10-12 for 11 char youtube id.
	        $%x'
	        ;
	    $result = preg_match($pattern, $url, $matches);
	    if ($result) {
	        return $matches[1];
	    }
	    return false;
    }

    public function edit($id)
	{
        $data['view'] = 'admin/wisata/edit';
        $data['dataKategori'] = $this->Kategori_model->read();        
		$data['wisata'] = $this->Wisata_model->read("wisata.id = '$id'")[0];
		$this->load->view('admin', $data);
    }
    
    public function show($id)
    {
        $data['wisata'] = $this->Wisata_model->read("wisata.id = '$id'")[0];
        $data['view'] = 'admin/wisata/show';
		$this->load->view('admin', $data);
    }

	public function update($id)
	{
		$config['upload_path'] = './uploads/wisata/';
        $config['allowed_types'] = 'gif|jpeg|jpg|png';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('surat_izin_usaha')){
            echo "errors";
        }
        else {
			$this->db->select("id,surat_izin_usaha");
	        $this->db->from("wisata");
	        $this->db->where("id = '$id'");
	        $query = $this->db->get();

	        if($query->num_rows() > 0) {
	            foreach($query->result() as $r) {
					$surat_izin_usaha		= $r->surat_izin_usaha;
	        	}
	            $dir = './uploads/wisata/';
	            opendir($dir);
	            unlink($dir.$surat_izin_usaha);
	        }

	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'surat_izin_usaha'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

		if (!$this->upload->do_upload('foto')){
            echo "errors";
        }
        else {
			$this->db->select("id,foto");
	        $this->db->from("wisata");
	        $this->db->where("id = '$id'");
	        $query = $this->db->get();

	        if($query->num_rows() > 0) {
	            foreach($query->result() as $r) {
					$foto		= $r->foto;
	        	}
	            $dir = './uploads/wisata/';
	            opendir($dir);
	            unlink($dir.$foto);
	        }

	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

		if (!$this->upload->do_upload('foto_2')){
            echo "errors";
        }
        else {
	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_2'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_3')){
            echo "errors";
        }
        else {

	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_3'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_4')){
            echo "errors";
        }
        else {
			
	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_4'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_5')){
            echo "errors";
        }
        else {
			
	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_5'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_6')){
            echo "errors";
        }
        else {

	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_6'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_7')){
            echo "errors";
        }
        else {
	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_7'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_2')){
            echo "errors";
        }
        else {
	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_8'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_9')){
            echo "errors";
        }
        else {
			
	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_9'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

        if (!$this->upload->do_upload('foto_2')){
            echo "errors";
        }
        else {
	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto_10'	=> $upload_content['file_name'],
	            );
	        $this->Wisata_model->update("id = '$id'",$data);
        }

		$data	= array(
			'nama' => $this->input->post("nama"),
			'id_kategori' => $this->input->post("id_kategori"),
            'status' => $this->input->post("status"),
            'id_user' => $this->session->userdata("id_user"),
            'deskripsi'	=> $this->input->post("deskripsi"),
            'harga_tiket'	=> $this->input->post("harga_tiket"),
            'waktu_upload' => date('y-m-d h:i:s'),
            'google_maps' => $this->input->post("google_maps"),
            'link_youtube' => $this->input->post("link_youtube"),
            'id_youtube' => $this->link_youtube($this->input->post("link_youtube")),
            'alasan' => $this->input->post("alasan"),
		);

		$this->Wisata_model->update("id = '$id'",$data);

        redirect("auth/wisata");
    }
    
    public function delete($id)
    {
        $this->db->select("id,foto");
        $this->db->from("wisata");
        $this->db->where("id = '$id'");
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            foreach($query->result() as $r) {
                $foto		= $r->foto;
            }
            $dir = './uploads/wisata/';
            opendir($dir);
            unlink($dir.$foto);
        }

        $this->Wisata_model->delete("wisata.id = '$id'");

        redirect("auth/wisata");
	}
	
	public function profil()
	{
		$data['view'] = 'admin/wisata/profil';
		$this->load->view('admin', $data);
	}

	public function storeProfil()
	{
		if(!$this->input->post('foto')) {
			$config['upload_path']		= './uploads/foto';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto')) {
				echo "errors";
			}
			else
			{
				$upload_foto = $this->upload->data();
				$foto = $upload_foto['file_name'];
			}
		}

		if(!$this->input->post('foto_ktp')) {
			$config['upload_path']		= './uploads/foto_ktp';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto_ktp')) {
				echo "errors";
			}
			else
			{
				$upload_foto_ktp = $this->upload->data();
				$foto_ktp = $upload_foto_ktp['file_name'];
			}
		}

		if(!$this->input->post('surat_izin_usaha')) {
			$config['upload_path']		= './uploads/surat_izin_usaha';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('surat_izin_usaha')) {
				echo "errors";
			}
			else
			{
				$upload_surat_izin = $this->upload->data();
				$surat_izin_usaha = $upload_surat_izin['file_name'];
			}
		}

		$detailUser	= array(
			'id_user' => $this->session->userdata("id_user"),
			'foto' => $foto,
			'foto_ktp' => $foto_ktp,
			'alamat' => $this->input->post('alamat'),
			'surat_izin_usaha' => $surat_izin_usaha,
			'email' => $this->input->post('email'),
			'no_hp' => $this->input->post('no_hp')
		);

		$this->Detail_user_model->create($detailUser);

		redirect("auth/wisata");
	}
}
