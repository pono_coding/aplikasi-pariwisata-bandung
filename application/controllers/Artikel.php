<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Artikel extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if(!$this->session->userdata("username")) redirect("login");
        $this->load->model('Artikel_model');
	}
	
	public function index()
	{
        $data['dataArtikel'] = $this->Artikel_model->read("","artikel.id DESC");
        $data['view'] = 'admin/artikel/index';
        $this->load->view('admin', $data);
    }
    
    public function create()
    {
        $data['view'] = 'admin/artikel/create';
        $this->load->view('admin', $data);
    }

    public function store() 
    {  
        if(!$this->input->post('foto')) {
			$config['upload_path']		= './uploads/artikel';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto')) {
                echo "errors";
			}
			else
			{
				$upload_data = $this->upload->data();
				$foto = $upload_data['file_name'];
			}
		}

		$data = array(
			'nama' => $this->input->post("nama"),
			'foto' => $foto,
			'deskripsi'	=> $this->input->post("deskripsi"),
			'waktu_upload' => date('y-m-d h:i:s'),
			'id_user' => $this->session->userdata("id_user")
		);

		$this->Artikel_model->create($data);

		redirect("auth/artikel");
    }

    public function edit($id)
	{
        $data['view'] = 'admin/artikel/edit';
		$data['artikel'] = $this->Artikel_model->read("artikel.id = '$id'")[0];
		$this->load->view('admin', $data);
    }
    
    public function show($id)
    {
        $data['artikel'] = $this->Artikel_model->read("artikel.id = '$id'")[0];
        $data['view'] = 'admin/artikel/show';
		$this->load->view('admin', $data);
    }

	public function update($id)
	{
		$config['upload_path'] = './uploads/artikel/';
        $config['allowed_types'] = 'gif|jpeg|jpg|png';

        $this->load->library('upload', $config);
		if (!$this->upload->do_upload('foto')){
            echo "errors";
        }
        else
		{
			$this->db->select("id,foto");
	        $this->db->from("artikel");
	        $this->db->where("id = '$id'");
	        $query = $this->db->get();

	        if($query->num_rows() > 0) {
	            foreach($query->result() as $r) {
					$foto		= $r->foto;
	        	}
	            $dir = './uploads/artikel/';
	            opendir($dir);
	            unlink($dir.$foto);
	        }

	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto'	=> $upload_content['file_name'],
	            );
	        $this->Artikel_model->update("id = '$id'",$data);
        }

		$data	= array(
			'nama' => $this->input->post("nama"),
			'deskripsi'	=> $this->input->post("deskripsi"),
        );

		$this->Artikel_model->update("id = '$id'",$data);

        redirect("auth/artikel");
    }
    
    public function delete($id)
    {
        $this->db->select("id,foto");
        $this->db->from("artikel");
        $this->db->where("id = '$id'");
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            foreach($query->result() as $r) {
                $foto		= $r->foto;
            }
            $dir = './uploads/artikel/';
            opendir($dir);
            unlink($dir.$foto);
        }

        $this->Artikel_model->delete("artikel.id = '$id'");

        redirect("auth/artikel");
    }
}
