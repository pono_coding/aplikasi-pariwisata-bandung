<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if(!$this->session->userdata("username")) redirect("login");
        $this->load->model('Kategori_model');
	}
	
	public function index()
	{
        $data['dataKategori'] = $this->Kategori_model->read("","id DESC");
        $data['view'] = 'admin/kategori/index';
        $this->load->view('admin', $data);
    }
    
    public function create()
    {
        $data['view'] = 'admin/kategori/create';
        $this->load->view('admin', $data);
    }

    public function store() 
    {  
        $storeKategori = array(
            'nama_kategori' => $this->input->post('nama_kategori'),
        );

        $this->Kategori_model->create($storeKategori);
        redirect('auth/kategori');

    }

    public function edit($id)
	{
		$data['view'] = 'admin/kategori/edit';
		$data['kategori'] = $this->Kategori_model->read("id = '$id'")[0];
		$this->load->view('admin', $data);
	}

	public function update($id)
	{
		$dataUpdate = array(
			'nama_kategori' => $this->input->post('nama_kategori')
		);

		$this->Kategori_model->update("id = '$id'", $dataUpdate);
		redirect('auth/kategori');
    }
    
    public function delete($id)
    {
        $this->Kategori_model->delete("id = '$id'");
        redirect('auth/kategori');
    }
        
}
