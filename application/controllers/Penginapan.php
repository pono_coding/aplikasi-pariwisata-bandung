<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penginapan extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        if(!$this->session->userdata("username")) redirect("login");
        $this->load->model('Penginapan_model');
	}
	
	public function index()
	{
        $data['dataPenginapan'] = $this->Penginapan_model->read("","penginapan.id DESC");
        $data['view'] = 'admin/penginapan/index';
        $this->load->view('admin', $data);
    }
    
    public function create()
    {
        $data['view'] = 'admin/penginapan/create';
        $this->load->view('admin', $data);
    }

    public function store() 
    {  
        if(!$this->input->post('foto')) {
			$config['upload_path']		= './uploads/penginapan';
			$config['allowed_types']	= 'gif|jpeg|jpg|png';

			$this->load->library('upload',$config);

			if(!$this->upload->do_upload('foto')) {
                echo "errors";
			}
			else
			{
				$upload_data = $this->upload->data();
				$foto = $upload_data['file_name'];
			}
		}

		$data = array(
            'foto' => $foto,
			'nama' => $this->input->post("nama"),
            'deskripsi'	=> $this->input->post("deskripsi"),
            'kategori'	=> $this->input->post("kategori"),
            'waktu_upload' => date('Y-m-d h:i:s'),
			'google_maps'	=> $this->input->post("google_maps"),
            'harga'	=> $this->input->post("harga"),
		);

		$this->Penginapan_model->create($data);

		redirect("auth/penginapan");
    }

    public function edit($id)
	{
        $data['view'] = 'admin/penginapan/edit';
		$data['penginapan'] = $this->Penginapan_model->read("penginapan.id = '$id'")[0];
		$this->load->view('admin', $data);
    }
    
    public function show($id)
    {
        $data['penginapan'] = $this->Penginapan_model->read("penginapan.id = '$id'")[0];
        $data['view'] = 'admin/penginapan/show';
		$this->load->view('admin', $data);
    }

	public function update($id)
	{
		$config['upload_path'] = './uploads/penginapan/';
        $config['allowed_types'] = 'gif|jpeg|jpg|png';

        $this->load->library('upload', $config);
		if (!$this->upload->do_upload('foto')){
            echo "errors";
        }
        else
		{
			$this->db->select("id,foto");
	        $this->db->from("penginapan");
	        $this->db->where("id = '$id'");
	        $query = $this->db->get();

	        if($query->num_rows() > 0) {
	            foreach($query->result() as $r) {
					$foto		= $r->foto;
	        	}
	            $dir = './uploads/penginapan/';
	            opendir($dir);
	            unlink($dir.$foto);
	        }

	        $upload_content 	= $this->upload->data();
	        $data = array(
	            	'foto'	=> $upload_content['file_name'],
	            );
	        $this->Penginapan_model->update("id = '$id'",$data);
        }

		$data	= array(
			'nama' => $this->input->post("nama"),
            'deskripsi'	=> $this->input->post("deskripsi"),
            'kategori'	=> $this->input->post("kategori"),
            'waktu_upload' => date('Y-m-d h:i:s'),
			'google_maps'	=> $this->input->post("google_maps"),
            'harga'	=> $this->input->post("harga"),
        );

		$this->Penginapan_model->update("id = '$id'",$data);

        redirect("auth/penginapan");
    }
    
    public function delete($id)
    {
        $this->db->select("id,foto");
        $this->db->from("penginapan");
        $this->db->where("id = '$id'");
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            foreach($query->result() as $r) {
                $foto		= $r->foto;
            }
            $dir = './uploads/penginapan/';
            opendir($dir);
            unlink($dir.$foto);
        }

        $this->Penginapan_model->delete("penginapan.id = '$id'");

        redirect("auth/penginapan");
    }
}
