<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            if(!$this->session->userdata("username")) redirect("login");
            $this->load->model('Wisata_model');
            $this->load->model('Event_model');
            $this->load->model('Artikel_model');
    }

    public function index()
    {
        $this->db->select("SUM(hits) as hits, tanggal");
        $this->db->group_by("tanggal");
        $dataPengunjung = $this->db->get("statistik_pengunjung")->result();

        $data['view'] = 'admin/dashboard/index';
        if($this->session->userdata("id_level") == 1) {
            $data['dataWisata'] = $this->Wisata_model->read("","wisata.id DESC",5);
            $data['dataPengujung'] =$dataPengunjung;
        } else {
            $id_user = $this->session->userdata("id_user");
            $data['dataWisata'] = $this->Wisata_model->read("wisata.id_user = '$id_user'","wisata.id DESC");
        }
        $data['dataEvent'] = $this->Event_model->read("","id DESC",5);
        $data['dataArtikel'] = $this->Artikel_model->read("","artikel.id DESC",5);
        $this->load->view('admin', $data);
    }
}
