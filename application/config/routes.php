<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// artikel
$route['artikel'] = 'Home/artikel';
$route['artikel/detail_artikel/(:num)'] = 'Home/detail_artikel/$1';

// penginapan
$route['penginapan'] = 'Home/penginapan';
$route['penginapan/detail_penginapan/(:num)'] = 'Home/detail_penginapan/$1';
$route['search_penginapan'] = 'Home/search_penginapan';


// event
$route['event'] = 'Home/event';
$route['event/detail_event/(:num)'] = 'Home/detail_event/$1';

// wisata
$route['wisata'] = 'Home/wisata';
$route['search_wisata'] = 'Home/search_wisata';
$route['wisata/detail_wisata/(:num)'] = 'Home/detail_wisata/$1';
$route['kategori_wisata/(:num)'] = 'Home/kategori_wisata/$1';

// kategori
$route['auth/kategori'] = 'kategori/index';
$route['auth/kategori/edit/(:num)'] = 'kategori/edit/$1';
$route['auth/kategori/update/(:num)'] = 'kategori/update/$1';
$route['auth/kategori/create'] = 'kategori/create';
$route['auth/kategori/store'] = 'kategori/store';
$route['auth/kategori/delete/(:num)'] = 'kategori/delete/$1';

// user
$route['auth/user'] = 'user/index';
$route['auth/user/delete/(:num)'] = 'user/delete/$1';
$route['auth/user/show/(:num)'] = 'user/show/$1';
$route['auth/user/acc/(:num)'] = 'user/acc/$1';


// auth
$route['auth/login'] = 'login/index';
$route['auth/registrasi'] = 'login/registrasi';
$route['auth/login/do_login'] = 'login/do_login';
$route['auth/registrasi/do_registrasi'] = 'login/do_registrasi';
$route['auth/login/logout'] = 'login/logout';

// dashboard
$route['auth/dashboard'] = 'dashboard/index';

// wisata
$route['auth/wisata'] = 'wisata/index';
$route['auth/wisata/edit/(:num)'] = 'wisata/edit/$1';
$route['auth/wisata/update/(:num)'] = 'wisata/update/$1';
$route['auth/wisata/create'] = 'wisata/create';
$route['auth/profil'] = 'wisata/profil';
$route['auth/storeProfil'] = 'wisata/storeProfil';
$route['auth/wisata/store'] = 'wisata/store';
$route['auth/wisata/delete/(:num)'] = 'wisata/delete/$1';
$route['auth/wisata/show/(:num)'] = 'wisata/show/$1';

// artikel
$route['auth/artikel'] = 'artikel/index';
$route['auth/artikel/edit/(:num)'] = 'artikel/edit/$1';
$route['auth/artikel/update/(:num)'] = 'artikel/update/$1';
$route['auth/artikel/create'] = 'artikel/create';
$route['auth/artikel/store'] = 'artikel/store';
$route['auth/artikel/delete/(:num)'] = 'artikel/delete/$1';
$route['auth/artikel/show/(:num)'] = 'artikel/show/$1';

// penginapan
$route['auth/penginapan'] = 'penginapan/index';
$route['auth/penginapan/edit/(:num)'] = 'penginapan/edit/$1';
$route['auth/penginapan/update/(:num)'] = 'penginapan/update/$1';
$route['auth/penginapan/create'] = 'penginapan/create';
$route['auth/penginapan/store'] = 'penginapan/store';
$route['auth/penginapan/delete/(:num)'] = 'penginapan/delete/$1';
$route['auth/penginapan/show/(:num)'] = 'penginapan/show/$1';

// artikel
$route['auth/event'] = 'event/index';
$route['auth/event/edit/(:num)'] = 'event/edit/$1';
$route['auth/event/update/(:num)'] = 'event/update/$1';
$route['auth/event/create'] = 'event/create';
$route['auth/event/store'] = 'event/store';
$route['auth/event/delete/(:num)'] = 'event/delete/$1';
$route['auth/event/show/(:num)'] = 'event/show/$1';

