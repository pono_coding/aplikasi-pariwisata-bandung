<section class="site-hero site-sm-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?= base_url("assets/user/images/wisata_bandung.jpg") ?>);">
      <div class="container">
        <div class="row align-items-center justify-content-center site-hero-sm-inner">
          <div class="col-md-7 text-center">
  
            <div class="mb-5 element-animate">
              <h1 class="mb-2">Search Wisata</h1>
              <!-- <p class="bcrumb"><a href="<?= site_url('/') ?>">Home</a> <span class="sep ion-android-arrow-dropright px-2"></span>  <span class="current">Wisata</span></p> -->
            </div>
            
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          
          <div class="col-md-6 col-lg-8 order-md-2">
            <div class="row align-items-center justify-content-center site-hero-inner">
              <div class="col-md-10">
                <div class="mb-5 element-animate">
                  <div class="block-17">
                    <form action="<?= site_url('search_wisata') ?>" method="get" class="d-block d-lg-flex mb-4">
                      <div class="fields d-block d-lg-flex">
                        <div class="textfield-search one-third"><input type="number" name="harga" value="" class="form-control" placeholder="Harga" required></div>
                        <div class="select-wrap one-third">
                          <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                          <select name="id_kategori[]" id="" class="form-control js-example-basic-multiple" required multiple="multiple" placeholder="Pilih Kategori">

                            <option value="" disabled>[ Kategori Wisata ]</option>
                            <?php
                              foreach($data_kategori as $kategori) {
                                ?>
                                  <option value="<?= $kategori->id ?>"><?= $kategori->nama_kategori ?></option>
                                <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <input type="submit" class="search-submit btn btn-primary" value="Search">  
                    </form>
                  </div>
                </div>
                
              </div>
            </div>
            <div class="row">
              <?php
                if(empty($data_wisata)) {
                    ?>
                        <center><h4>Data Wisata Kosong</h4></center>
                    <?php
                } else {
                    ?>
                    <?php
                    foreach($data_wisata as $wisata) {
                        ?>
                          <div class="col-md-12 col-lg-6 mb-5">
                            <div class="block-20 ">
                              <figure>
                                <a href="<?= site_url('wisata/detail_wisata/'.$wisata->id_wisata) ?>"><img src="<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>" alt="" class="img-fluid"></a>
                              </figure>
                              <div class="text">
                                <h3 class="heading"><a href="<?= site_url('wisata/detail_wisata/'.$wisata->id_wisata) ?>"><?= $wisata->nama_wisata ?></a></h3>
                                <div class="meta">
                                  <div><span class="ion-android-calendar"></span> <?= date('d F Y', strtotime($wisata->waktu_upload)) ?></div>
                                  <div><span class="ion-android-person"></span> <?= $wisata->user_nama_lengkap ?></div>
                                  <div><span class="ion-android-person"></span> Harga Tiket: Rp. <?= number_format($wisata->harga_tiket) ?></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        <?php
                      }
                }
              ?>
            </div>

            <!-- <div class="row mb-5">
              <div class="col-md-12 text-center">
                <div class="block-27">
                  <ul>
                    <li><a href="#">&lt;</a></li>
                    <li class="active"><span>1</span></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">&gt;</a></li>
                  </ul>
                </div>
              </div>
            </div> -->
          </div>
          <!-- END content -->
          <div class="col-md-6 col-lg-4 order-md-1">
            <div class="block-25 mb-5">
              <div class="heading">wisata Lainnya</div>
              <ul>
                <?php
                  foreach($data_wisata_recent as $wisata) {
                    ?>
                      <li>
                        <a href="<?= site_url('wisata/detail_wisata/'.$wisata->id_wisata) ?>" class="d-flex">
                          <figure class="image mr-3">
                            <img src="<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>" alt="" class="img-fluid">
                          </figure>
                          <div class="text">
                            <h3 class="heading"><?= $wisata->nama_wisata ?></h3>
                          </div>
                        </a>
                      </li>
                    <?php
                  }
                ?>
              </ul>
            </div>
          </div>
          <!-- END Sidebar -->
        </div>
      </div>
    </div>

    <div class="py-5 block-22">
        <div class="container">
            <div class="row align-items-center">
            <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
                <h2 class="heading">Menarik dengan konten kami ?</h2>
                <p>Silahkan daftarkan diri anda dan jadilah salah satu kontributor dalam konten wisata bandung</p>
            </div>
            <div class="col-md-6">
                <form action="#" class="subscribe">
                <div class="form-group">
                    <a style="background-color:#fff; color:#11cbd7;" class="btn btn-primary submit" href="<?= site_url('auth/registrasi') ?>">Registrasi</a>
                </div>
                </form>
            </div>
            </div>
        </div>
        </div>