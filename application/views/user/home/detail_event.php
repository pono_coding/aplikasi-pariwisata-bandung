<section class="site-hero site-sm-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?= base_url('uploads/event/'.$event->foto) ?>);">
      <div class="container">
        <div class="row align-items-center justify-content-center site-hero-sm-inner">
          <div class="col-md-7 text-center">
  
            <div class="mb-5 element-animate">
              <h1 class="mb-2"><?= $event->nama ?></h1>
              <p class="bcrumb"><a href="<?= site_url('/') ?>">Home</a> <span class="sep ion-android-arrow-dropright px-2"></span>  <span class="current">Event</span></p>
            </div>
            
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          
          <div class="col-md-12 col-lg-8 order-md-2">
            <div class="row">
              <div class="col-md-12 col-lg-12 mb-5">
                  <div class="block-20 ">
                    <figure>
                      <a href="<?= site_url('event/detail_event/'.$event->id) ?>"><img src="<?= base_url('uploads/event/'.$event->foto) ?>" alt="" width="100%"; class="img-fluid"></a>
                    </figure>
                    <div class="text">
                      <h3 class="heading"><a href="<?= site_url('event/detail_event/'.$event->id) ?>"><?= $event->nama ?></a></h3>
                      <div class="meta">
           
                      </div>
                      <p>
                        <?= $event->deskripsi ?>
                      </p>
                    </div>
                  </div>
                </div>
            </div>
            <div id="disqus_thread"></div>
          </div>
          <!-- END content -->
          <div class="col-md-6 col-lg-4 order-md-1">
            <div class="block-25 mb-5">
              <div class="heading">Event Lainnya</div>
              <ul>
                <?php
                  foreach($data_event_recent as $event) {
                    ?>
                      <li>
                        <a href="<?= site_url('event/detail_event/'.$event->id) ?>" class="d-flex">
                            <figure class="image mr-3">
                            <img src="<?= base_url('uploads/event/'.$event->foto) ?>" alt="" class="img-fluid">
                            </figure>
                            <div class="text">
                              <h3 class="heading"><?= $event->nama ?></h3>
                            </div>
                        </a>
                      </li>
                    <?php
                  }
                ?>
              </ul>
            </div>
          </div>
          <!-- END Sidebar -->
        </div>
      </div>
    </div>

    <div class="py-5 block-22">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
            <h2 class="heading">Menarik dengan konten kami ?</h2>
            <p>Silahkan daftarkan diri anda dan jadilah salah satu kontributor dalam konten wisata bandung</p>
          </div>
          <div class="col-md-6">
            <form action="#" class="subscribe">
              <div class="form-group">
                <a style="background-color:#fff; color:#11cbd7;" class="btn btn-primary submit" href="<?= site_url('auth/registrasi') ?>">Registrasi</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>