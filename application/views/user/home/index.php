<section class="site-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?= base_url('assets/user/images/big_image_2.jpg') ?>);">
  <div class="container">
    <div class="row align-items-center justify-content-center site-hero-inner">
      <div class="col-md-10">

        <div class="mb-5 element-animate">
          <div class="block-17">
            <h2 class="heading text-center mb-4">Cari Tempat Wisata Favoritmu Disini!</h2>
            <form action="<?= site_url('search_wisata') ?>" method="get" class="d-block d-lg-flex mb-4">
              <div class="fields d-block d-lg-flex">
                <div class="textfield-search one-third"><input type="number" name="harga" class="form-control" placeholder="Cari Berdasarkan Harga..." required></div>
                <div class="select-wrap one-third">
                  <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                  <select name="id_kategori[]" id="" class="form-control js-example-basic-multiple" required multiple="multiple" placeholder="Pilih Kategori">
                    <option value="">[ Kategori Wisata ]</option>
                    <?php
                      foreach($data_kategori as $kategori) {
                        ?>
                          <option value="<?= $kategori->id ?>"><?= $kategori->nama_kategori ?></option>
                        <?php
                      }
                    ?>
                  </select>
                </div>
              </div>
              <input type="submit" class="search-submit btn btn-primary" value="Search">  
            </form>
            <p class="text-center mb-5">ada <?= count($jumlah_wisata) ?> tempat wisata dibandung yang menjadi rekomendasi untuk liburan anda</p>
            <p class="text-center"><a href="<?= site_url('wisata') ?>" class="btn py-3 px-5">Lihat Selengkapnya</a></p>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</section>
<!-- END section -->

<section class="site-section element-animate">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6 order-md-2">
        <div class="block-16">
          <figure>
            <img src="https://i.ytimg.com/vi/G-eqQBYmhO0/maxresdefault.jpg" alt="Image placeholder" class="img-fluid">
            <a href="https://www.youtube.com/watch?v=G-eqQBYmhO0" class="play-button popup-vimeo"><span class="ion-ios-play"></span></a>

            <!-- <a href="https://vimeo.com/45830194" class="button popup-vimeo" data-aos="fade-right" data-aos-delay="700"><span class="ion-ios-play"></span></a> -->

          </figure>
        </div>
      </div>
      <div class="col-md-6 order-md-1">

        <div class="block-15">
          <div class="heading">
            <h2>Selamat Datang di Wisata Bandung</h2>
          </div>
          <div class="text mb-5">
          <p>Kota Bandung merupakan kota metropolitan terbesar di Provinsi Jawa Barat, sekaligus menjadi ibu kota provinsi tersebut. Kota ini terletak 140 km sebelah tenggara Jakarta, dan merupakan kota terbesar di wilayah Pulau Jawa bagian selatan</p>
          </div>
          <p><a href="#" class="btn btn-primary reverse py-2 px-4">Read More</a></p>
          
        </div>

      </div>
      
    </div>

  </div>
</section>
<!-- END section -->

<section class="site-section pt-3 element-animate">
  <div class="container">
    <div class="row">
      <div class="col-md-6 col-lg-3">
        <div class="media block-6 d-block">
          <div class="icon mb-3"><span class="flaticon-book"></span></div>
          <div class="media-body">
            <h3 class="heading">Karakter</h3>
            <p>Membangun Masyarakat yang humanis, agamis, berkualitas dan berdaya saing</p>
          </div>
        </div> 
      </div>
      <div class="col-md-6 col-lg-3">
        <div class="media block-6 d-block">
          <div class="icon mb-3"><span class="flaticon-student"></span></div>
          <div class="media-body">
            <h3 class="heading">Tata Kelola</h3>
            <p>Mewujudkan Tata Kelola Pemerintahan yang Efektif, Efisien, Bersih dan Melayani</p>
          </div>
        </div> 
      </div>
      
      <div class="col-md-6 col-lg-3">
        <div class="media block-6 d-block">
          <div class="icon mb-3"><span class="flaticon-diploma"></span></div>
          <div class="media-body">
            <h3 class="heading">Ekonomi</h3>
            <p>Membangun Perekonomian yang Mandiri, Kokoh, dan Berkeadilan</p>
          </div>
        </div> 
      </div>
      <div class="col-md-6 col-lg-3">
        <div class="media block-6 d-block">
          <div class="icon mb-3"><span class="flaticon-professor"></span></div>
          <div class="media-body">
            <h3 class="heading">Tata ruang</h3>
            <p>Mewujudkan Bandung nyaman melalui perencanaan tata ruang.</p>
          </div>
        </div> 
      </div>
    </div>
  </div>
</section>
<!-- END section -->

<section class="site-section bg-light element-animate" id="section-counter">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <figure><img src="<?= base_url('assets/user/images/img_2_b.jpg') ?>" alt="Image placeholder" class="img-fluid"></figure>
      </div>
      <div class="col-lg-5 ml-auto">
        <div class="block-15">
          <div class="heading">
            <h2>Wisata Kota Bandung</h2>
          </div>
          <div class="text mb-5">
            <p>Bandung merupakan salah satu kota terbesar di indonesia. Bandung dijuluki sebagai Paris Van Java. Semua ada di Bandung. Masyarakatnya yang ramah membuat Bandung menjadi salah satu kota dengan ketertiban yang paling tinggi. tata kota yang apik dan fasilitas yang memadai membuat kota bandung semakin maju.</p>
          </div>
          </div>
      </div>
    </div>
  </div>
</section>
<!-- END section -->

<div class="site-section bg-light">
  <div class="container">
    <div class="row justify-content-center mb-5 element-animate">
      <div class="col-md-7 text-center section-heading">
        <h2 class="text-primary heading">Wisata Popular Di Bandung</h2>
        <p>Tidak hanya sebagai kota megropolitan, ada banyak sekali tempat wisata di bandung yang tidak kalah dengan tempat-tempat lainnya</p>
        <p><a href="<?= site_url('wisata') ?>" class="btn btn-primary py-2 px-4">Lihat Selengkapnya</a></p>
      </div>
    </div>
  </div>
  <div class="container-fluid block-11 element-animate">
    <div class="nonloop-block-11 owl-carousel">
      <?php
        foreach($data_wisata as $wisata) {
          ?>
            <div class="item">
              <div class="block-19">
                  <figure>
                    <img src="<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>" alt="Image" class="img-fluid">
                  </figure>
                  <div class="text">
                    <h2 class="heading"><a href="<?= site_url('wisata/detail_wisata/'.$wisata->id_wisata) ?>"><?= $wisata->nama_wisata ?></a></h2>
                    <p class="mb-4">
                      <?php
                        if(strlen(strip_tags($wisata->deskripsi_wisata)) > 150) {
                          echo substr(strip_tags($wisata->deskripsi_wisata), 0,150)."....";
                        } else {
                          echo strip_tags($wisata->deskripsi_wisata);
                        }
                      ?>
                      </p>
                    <div class="meta d-flex align-items-center">
                      <div class="number">
                        <span>Harga Tiket: Rp<?= number_format($wisata->harga_tiket) ?></span>
                      </div>
                      <a class="price text-right" href="<?= site_url('wisata/detail_wisata/'.$wisata->id_wisata) ?>"><span>Lihat Selengkapnya</span></a>
                    </div>
                  </div>
                </div>
            </div>
          <?php
        }
      ?>      
    </div>
  </div>

  
</div>
<!-- END section -->


<div class="container site-section element-animate">
  <div class="row justify-content-center mb-5">
      <div class="col-md-7 text-center section-heading">
        <h2 class="text-primary heading">Testimoni</h2>
        <p>wisata bandung menarik perhatian masyarakat. berikut pesan dan kesan dari beberapa pengujung wisata di bandung</p>
      </div>
    </div>
  <div class="row">
    <div class="col-md-6 col-lg-4">
      <div class="block-2">
        <div class="flipper">
          <div class="front" style="background-image: url(<?= base_url('assets/user/images/raisa.jpg') ?>);">
            <div class="box">
              <h2>Raisa</h2>
              <p>Artis/Penyanyi</p>
            </div>
          </div>
          <div class="back">
            <!-- back content -->
            <blockquote>
              <p>&ldquo;Ciwidey, bagian selatan Bandung, memiliki pesonanya tersendiri. Alam yang asri dan nuansa natural yang menyenangkan, rugi kalau tidak berkunjung ke destinasi wisata di kawasan ini saat liburan ke Bandung. .&rdquo;</p>
            </blockquote>
            <div class="author d-flex">
              <div class="image mr-3 align-self-center">
                <img src="<?= base_url('assets/user/images/raisa.jpg') ?>" alt="">
              </div>
              <div class="name align-self-center">Raisa <span class="position">Artis/Penyanyi</span></div>
            </div>
          </div>
        </div>
      </div> <!-- .flip-container -->
    </div>

    <div class="col-md-6 col-lg-4">
      <div class="block-2 ">
        <div class="flipper">
          <div class="front" style="background-image: url(<?= base_url('assets/user/images/vino.jpg') ?>);">
            <div class="box">
              <h2>Vino G Bastian</h2>
              <p>Aktor</p>
            </div>
          </div>
          <div class="back">
            <!-- back content -->
            <blockquote>
              <p>&ldquo;Saya menyukai Bandung karena tempat ini menawarkan beragam hiburan. Mulai dari melihat pemandangan di ketinggian, nongkrong di cafe unik yang lagi hits di kalangan anak muda, mau belanja oleh-oleh juga bisa.&rdquo;</p>
            </blockquote>
            <div class="author d-flex">
              <div class="image mr-3 align-self-center">
                <img src="<?= base_url('assets/user/images/vino.jpg') ?>" alt="">
              </div>
              <div class="name align-self-center">Vino G Bastian <span class="position">Aktor</span></div>
            </div>
          </div>
        </div>
      </div> <!-- .flip-container -->
    </div>

    <div class="col-md-6 col-lg-4">
      <div class="block-2">
        <div class="flipper">
          <div class="front" style="background-image: url(<?= base_url('assets/user/images/iwan_fals.jpg') ?>);">
            <div class="box">
              <h2>Virgiawan Listanto</h2>
              <p>Musisi</p>
            </div>
          </div>
          <div class="back">
            <!-- back content -->
            <blockquote>
              <p>&ldquo;Bandung selalu ada saja wisata yang baru. Terakhir kali saya kesana adalah sekitar tahun 2013, saat itu sedang hebohnya pembukaan Trans Studio Bandung.&rdquo;</p>
            </blockquote>
            <div class="author d-flex">
              <div class="image mr-3 align-self-center">
                <img src="<?= base_url('assets/user/images/iwan_fals.jpg') ?>" alt="">
              </div>
              <div class="name align-self-center">Virgiawan Listanto <span class="position">Musisi</span></div>
            </div>
          </div>
        </div>
      </div> <!-- .flip-container -->
    </div>
  </div>
</div>
<!-- END .block-2 -->


<div class="site-section bg-light">
  <div class="container">
    <div class="row justify-content-center mb-5 element-animate">
      <div class="col-md-7 text-center section-heading">
        <h2 class="text-primary heading">Artikel</h2>
        <p>beberapa artikel tentang wisata bandung diantaranya:</p>
      </div>
    </div>
    <div class="row element-animate">
      
      <?php
        foreach($data_artikel as $value => $artikel) {
          if($value == 0) {
            ?>
              <div class="col-md-12 mb-5 mb-lg-0 col-lg-6">
                <div class="block-20 ">
                  <figure>
                    <a href="<?= site_url('artikel/detail_artikel/'.$artikel->id) ?>"><img src="<?= base_url('uploads/artikel/'.$artikel->foto) ?>" alt="" class="img-fluid"></a>
                  </figure>
                  <div class="text">
                    <h3 class="heading"><a href="<?= site_url('artikel/detail_artikel/'.$artikel->id) ?>"><?= $artikel->nama ?></a></h3>
                    <div class="meta">
                      <div><span class="ion-android-calendar"></span> <?= date('d F Y', strtotime($artikel->waktu_upload)) ?></div>
                      <div><span class="ion-android-person"></span> <?= $artikel->nama_lengkap ?></div>
                    </div>
                  </div>
                </div>
              </div>
            <?php
          }
        }
      ?>
      <div class="col-md-12 col-lg-6">

      <?php
        foreach($data_artikel as $value => $artikel) {
          if($value != 0) {
            ?>
              <div class="block-21 d-flex mb-4">
                <figure class="mr-3">
                  <a href="<?= site_url('artikel/detail_artikel/'.$artikel->id) ?>"><img src="<?= base_url('uploads/artikel/'.$artikel->foto) ?>" alt="" class="img-fluid"></a>
                </figure>
                <div class="text">
                  <h3 class="heading"><a href="<?= site_url('artikel/detail_artikel/'.$artikel->id) ?>"><?= $artikel->nama ?></a></h3>
                  <div class="meta">
                    <div><span class="ion-android-calendar"></span><?= date('d F Y', strtotime($artikel->waktu_upload)) ?></div>
                    <div><span class="ion-android-person"></span> <?= $artikel->nama_lengkap ?></div>
                  </div>
                </div>
              </div>  
            <?php
          }
        }
      ?>
      </div>
    </div>
  </div>
</div>

<div class="py-5 block-22">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
        <h2 class="heading">Menarik dengan konten kami ?</h2>
        <p>Silahkan daftarkan diri anda dan jadilah salah satu kontributor dalam konten wisata bandung</p>
      </div>
      <div class="col-md-6">
        <form action="#" class="subscribe">
          <div class="form-group">
            <a style="background-color:#fff; color:#11cbd7;" class="btn btn-primary submit" href="<?= site_url('auth/registrasi') ?>">Registrasi</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>