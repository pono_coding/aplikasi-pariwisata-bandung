<section class="site-hero site-sm-hero overlay" data-stellar-background-ratio="0.5" style="background-image: url(<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>);">
      <div class="container">
        <div class="row align-items-center justify-content-center site-hero-sm-inner">
          <div class="col-md-7 text-center">
  
            <div class="mb-5 element-animate">
              <h1 class="mb-2"><?= $wisata->nama_wisata ?></h1>
              <p class="bcrumb"><a href="<?= site_url('/') ?>">Home</a> <span class="sep ion-android-arrow-dropright px-2"></span>  <span class="current">Wisata</span></p>
            </div>
            
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          
          <div class="col-md-12 col-lg-8 order-md-2">
            <div class="row">
              <div class="col-md-12 col-lg-12 mb-5">
                  <div class="block-20 ">
                      <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                          <div class="item active">
                            <img src="<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>" alt="Los Angeles" style="width:100%;">
                          </div>

                          <?php
                            if(!empty($wisata->foto_2)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_2) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_3)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_3) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_4)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_4) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_5)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_5) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_6)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_6) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_7)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_7) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_8)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_8) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_9)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_9) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                          <?php
                            if(!empty($wisata->foto_10)) {
                              ?>
                                <div class="item">
                                  <img src="<?= base_url('uploads/wisata/'.$wisata->foto_10) ?>" alt="Chicago" style="width:100%;">
                                </div>
                              <?php
                            }
                          ?>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                    </div>
                    <div class="text">
                      <h3 class="heading"><a href="<?= site_url('wisata/detail_wisata/'.$wisata->id_wisata) ?>"><?= $wisata->nama_wisata ?></a></h3>
                      <div class="meta">
                        <div><span class="ion-android-calendar"></span> <?= date('d F Y', strtotime($wisata->waktu_upload)) ?></div>
                        <div><span class="ion-android-person"></span> <?= $wisata->user_nama_lengkap ?></div>
                        <div><span class="ion-android-person"></span> Harga Mulai Dari : Rp. <?= number_format($wisata->harga_tiket) ?></div>
                      </div>
                      <p>
                        <?= $wisata->deskripsi_wisata ?>
                      </p>
                    </div>
                    <?php
                      if(!empty($wisata->link_youtube)) {
                        ?>
                          <iframe width="100%" height="500px" src="https://www.youtube.com/embed/<?= $wisata->id_youtube ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <?php
                      }
                    ?>
                    <div class="iframe-content">
                      <?= $wisata->google_maps ?>
                    </div>
                  </div>
                </div>
            </div>

            <div id="disqus_thread"></div>
          </div>
          <!-- END content -->
          <div class="col-md-6 col-lg-4 order-md-1">
            <div class="block-25 mb-5">
              <div class="heading">wisata Lainnya</div>
              <ul>
                <?php
                  foreach($data_wisata_recent as $wisata) {
                    ?>
                      <li>
                        <a href="<?= site_url('wisata/detail_wisata/'.$wisata->id_wisata) ?>" class="d-flex">
                            <figure class="image mr-3">
                            <img src="<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>" alt="" class="img-fluid">
                            </figure>
                            <div class="text">
                              <h3 class="heading"><?= $wisata->nama_wisata ?></h3>
                            </div>
                        </a>
                      </li>
                    <?php
                  }
                ?>
              </ul>
            </div>
          </div>
          <!-- END Sidebar -->
        </div>
      </div>
  </div>

    <div class="py-5 block-22">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-md-6 mb-4 mb-md-0 pr-md-5">
            <h2 class="heading">Menarik dengan konten kami ?</h2>
            <p>Silahkan daftarkan diri anda dan jadilah salah satu kontributor dalam konten wisata bandung</p>
          </div>
          <div class="col-md-6">
            <form action="#" class="subscribe">
              <div class="form-group">
                <a style="background-color:#fff; color:#11cbd7;" class="btn btn-primary submit" href="<?= site_url('auth/registrasi') ?>">Registrasi</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>