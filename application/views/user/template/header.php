<!doctype html>
<html lang="en">
  <head>
    <title>Aplikasi Wisata Kota Bandung</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">

    <link rel="stylesheet" href="<?= base_url('assets/user/css/bootstrap.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user/css/animate.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user/css/owl.carousel.min.css') ?>">

    <link rel="stylesheet" href="<?= base_url('assets/user/fonts/ionicons/css/ionicons.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user/fonts/fontawesome/css/font-awesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user/fonts/flaticon/font/flaticon.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/user/css/magnific-popup.css') ?>">   
    <link rel="stylesheet" href="<?= base_url('assets/user/css/select2.min.css') ?>">   

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

    <!-- Theme Style -->
    <link rel="stylesheet" href="<?= base_url('assets/user/css/style.css') ?>">

    <style>
      .iframe-content iframe {
        width:100%;
      }

      .navbar {
        margin-bottom:0px;
      }
    </style>
  </head>
  <body>

  <header role="banner">
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
      <a class="navbar-brand absolute" href="<?= site_url('/') ?>">Wisata Bandung</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <?php
        $data_kategori = $this->db->get('kategori')->result();
      ?>
      <div class="collapse navbar-collapse navbar-light" id="navbarsExample05">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item">
            <a class="nav-link active" href="<?= site_url('/') ?>">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wisata</a>
            <div class="dropdown-menu" aria-labelledby="dropdown04">
              <?php
                foreach($data_kategori as $kategori) {
                  ?>
                    <a class="dropdown-item" href="<?= site_url('kategori_wisata/'.$kategori->id) ?>"><?= $kategori->nama_kategori ?></a>
                  <?php
                }
              ?>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link active" href="<?= site_url('event') ?>">Event</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('artikel') ?>">Artikel</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('penginapan') ?>">Penginapan</a>
          </li>
        </ul>
        <ul class="navbar-nav absolute-right">
          <li>
            <a href="<?= site_url('auth/login') ?>">Login</a> / <a href="<?= site_url('auth/registrasi') ?>">Register</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
</header>
<!-- END header -->
    
    
  
  