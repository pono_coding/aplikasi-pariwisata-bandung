<footer class="site-footer">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
            <h3>Wisata Bandung</h3>
            <p>Kota Bandung merupakan kota metropolitan terbesar di Provinsi Jawa Barat, dan merupakan kota terbesar di wilayah Pulau Jawa bagian selatan</p>
          </div>
          <div class="col-md-12 col-lg-3 mb-5 mb-lg-0">
            <h3 class="heading">Halaman Web</h3>
            <div class="row">
              <div class="col-md-6">
                <ul class="list-unstyled">
                  <li><a href="#">Home</a></li>
                  <li><a href="#">Wisata</a></li>
                  <li><a href="#">Event</a></li>
                  <li><a href="#">Artikel</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
            <h3 class="heading">Artikel</h3>

            
            <?php
            $data_artikel = $this->db->select('*')->order_by('id','DESC')->limit(3)->get('artikel')->result();
              foreach($data_artikel as $row => $artikel) {
                if($row <= 2) {
                  ?>
                  <div class="block-21 d-flex mb-4">
                    <div class="text">
                      <h3 class="heading mb-0"><a href="<?= site_url('artikel/detail_artikel/'.$artikel->id) ?>"><?= $artikel->nama ?></a></h3>
                      <div class="meta">
                        <div><span class="ion-android-calendar"></span> <?= date('d F Y',strtotime($artikel->waktu_upload)) ?></div>
                        <div><span class="ion-android-person"></span> Admin</div>
                      </div>
                    </div>
                  </div>  
                <?php
                }
              }
            ?>
          </div>
          <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
            <h3 class="heading">Informasi</h3>
            <div class="block-23">
              <ul>
                <li><span class="icon ion-android-pin"></span><span class="text"> JL. Ahmad Yani No.277, Babakan Surabaya, Kiaracondong, Cihapit, Bandung, Kota Bandung, Jawa Barat 40281</span></li>
                <li><a href="#"><span class="icon ion-ios-telephone"></span><span class="text">(022) 7271724</span></a></li>
                <li><a href="#"><span class="icon ion-android-mail"></span><span class="text">disbudpar@bandung.go.id</span></a></li>
                <li><span class="icon ion-android-time"></span><span class="text">Jam Pelayanan : 08.00 - 16.30 WIB</span></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="row pt-5">
          <div class="col-md-12 text-center copyright">
            <p class="float-md-right">
              <a href="#" class="fa fa-facebook p-2"></a>
              <a href="#" class="fa fa-twitter p-2"></a>
              <a href="#" class="fa fa-linkedin p-2"></a>
              <a href="#" class="fa fa-instagram p-2"></a>
            </p>
          </div>
        </div>
      </div>
    </footer>
    <!-- END footer -->
    
    <!-- loader -->
    <div id="loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#f4b214"/></svg></div>

    <script src="<?= base_url('assets/user/js/jquery-3.2.1.min.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/jquery-migrate-3.0.0.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/popper.min.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/bootstrap.min.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/owl.carousel.min.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/jquery.waypoints.min.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/jquery.stellar.min.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/jquery.animateNumber.min.js') ?>"></script>
    
    <script src="<?= base_url('assets/user/js/jquery.magnific-popup.min.js') ?>"></script>

    <script src="<?= base_url('assets/user/js/main.js') ?>"></script>
    <script src="<?= base_url('assets/user/js/select2.min.js') ?>"></script>

    <script>
      $(document).ready(function() {
          $('.js-example-basic-multiple').select2();
      });
    </script>

    

    <script>
          /**
          *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
          *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
          /*
          var disqus_config = function () {
          this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
          this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
          };
          */
          (function() { // DON'T EDIT BELOW THIS LINE
          var d = document, s = d.createElement('script');
          s.src = 'https://wisata-5.disqus.com/embed.js';
          s.setAttribute('data-timestamp', +new Date());
          (d.head || d.body).appendChild(s);
          })();
          </script>
          <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
          </div>
  </body>
</html>