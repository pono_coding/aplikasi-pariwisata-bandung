<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User
            <small>data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('auth/user') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <td><?= $dataUser->nama_lengkap ?></td>
                                </tr> 
                                <tr>
                                    <th>Email</th>
                                    <td><?= empty($detail_user->email) ? '-' :  $detail_user->email ?></td>
                                </tr> 
                                <tr>
                                    <th>Nomor Telepon</th>
                                    <td><?= empty($detail_user->no_hp) ? '-' : $detail_user->no_hp ?></td>
                                </tr> 
                                <tr>
                                    <th>Foto</th>
                                    <td>
                                        <?php
                                            if(empty($detail_user->foto)) {
                                                echo "-";
                                            } else {
                                                ?>
                                                    <img src="<?= base_url("uploads/foto/".$detail_user->foto) ?>" alt="" width="200px">
                                                <?php
                                            }
                                        ?>
                                    </td>
                                </tr>     
                                <tr>
                                    <th>Foto KTP</th>
                                    <td>
                                        <?php
                                            if(empty($detail_user->foto_ktp)) {
                                                echo "-";
                                            } else {
                                                ?>
                                                    <img src="<?= base_url("uploads/foto/".$detail_user->foto_ktp) ?>" alt="" width="200px">
                                                <?php
                                            }
                                        ?>
                                    </td>
                                </tr>       
                                <tr>
                                    <th>Surat Izin Usaha</th>
                                    <td>
                                    <?php
                                        if(empty($detail_user->surat_izin_usaha)) {
                                            echo "-";
                                        } else {
                                            ?>
                                                <img src="<?= base_url("uploads/foto/".$detail_user->surat_izin_usaha) ?>" alt="" width="200px">
                                            <?php
                                        }
                                    ?>
                                    </td>
                                </tr>                              
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->