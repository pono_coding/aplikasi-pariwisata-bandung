<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Wisata
            <small>detail data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('auth/wisata') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="" class="table table-bordered table-striped">
                            <tr>
                                <th width="200px">Nama Wisata</th>
                                <td width="50px">:</td>
                                <td><?= $wisata->nama_wisata ?></td>
                            </tr>

                            <tr>
                                <th>Foto</th>
                                <td>:</td>
                                <td>
                                    <img src="<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>" width="200px"/>
                                    <?php
                                        if(!empty($wisata->foto_2)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_wisata) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_3)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_3) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_4)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_4) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_5)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_5) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_6)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_6) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_7)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_7) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_8)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_8) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_9)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_9) ?>" width="200px"/>
                                            <?php
                                        }

                                        if(!empty($wisata->foto_10)) {
                                            ?>
                                                <img src="<?= base_url('uploads/wisata/'.$wisata->foto_10) ?>" width="200px"/>
                                            <?php
                                        }
                                    ?>
                                </td>
                            </tr>

                            <tr>
                                <th>Link Youtube</th>
                                <td>:</td>
                                <td><?= $wisata->link_youtube ?></td>
                            </tr>

                            <tr>
                                <th>Kategori Wisata</th>
                                <td>:</td>
                                <td><?= $wisata->kategori_wisata ?></td>
                            </tr>

                            <tr>
                                <th>Harga</th>
                                <td>:</td>
                                <td>Rp. <?= number_format($wisata->harga_tiket) ?></td>
                            </tr>

                            <tr>
                                <th>Link Google Maps</th>
                                <td>:</td>
                                <td><?= $wisata->google_maps ?></td>
                            </tr>

                            <tr>
                                <th>Posted By</th>
                                <td>:</td>
                                <td><?= $wisata->user_nama_lengkap ?></td>
                            </tr>

                            <tr>
                                <th>Deskripsi</th>
                                <td>:</td>
                                <td><?= $wisata->deskripsi_wisata ?></td>
                            </tr>
                            <tr>
                                <th>Alasan Tidak Diterima</th>
                                <td>:</td>
                                <td><?= empty($wisata->alasan) ? "-" : $wisata->alasan ?></td>
                            </tr>
                        </table>    
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->