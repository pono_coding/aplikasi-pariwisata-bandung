<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Wisata
          <small>Tambah Data</small>
      </h1>
  </section>

  <!-- Main content -->
  <section class="content">
        <div class="row">
            <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box">
                        <div class="box-header with-border">
                            <a href="<?= site_url('auth/wisata') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                        </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <form role="form" method="POST" action="<?= site_url('auth/wisata/store')?>" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nama Wisata</label>
                                    <input type="text" class="form-control" name="nama" placeholder="Nama Wisata" required>
                                </div>

                                <div class="form-group">
                                    <label>Kategori</label>
                                    <select name="id_kategori" id="" class="form-control" required>
                                        <option value="">[ Pilih Kategori ]</option>
                                        <?php
                                            foreach($dataKategori as $kategori) {
                                                ?>
                                                    <option value="<?= $kategori->id ?>"><?= $kategori->nama_kategori ?></option>
                                                <?php
                                            }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>Foto 1</label>
                                    <input type="file" class="form-control" name="foto" placeholder="Foto" required>
                                </div>

                                <div class="form-group">
                                    <label>Foto 2</label>
                                    <input type="file" class="form-control" name="foto_2" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 3</label>
                                    <input type="file" class="form-control" name="foto_3" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 4</label>
                                    <input type="file" class="form-control" name="foto_4" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 5</label>
                                    <input type="file" class="form-control" name="foto_5" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 6</label>
                                    <input type="file" class="form-control" name="foto_6" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 7</label>
                                    <input type="file" class="form-control" name="foto_7" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 8</label>
                                    <input type="file" class="form-control" name="foto_8" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 9</label>
                                    <input type="file" class="form-control" name="foto_9" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Foto 10</label>
                                    <input type="file" class="form-control" name="foto_10" placeholder="Foto">
                                </div>

                                <div class="form-group">
                                    <label>Surat Izin Usaha</label>
                                    <input type="file" class="form-control" name="surat_izin_usaha" placeholder="Surat Izin Usaha" required>
                                </div>
                            
                                <div class="form-group">
                                    <label>Masukan Harga (berdasarkan kategori yang dipilih)</label>
                                    <input type="number" class="form-control" name="harga_tiket" placeholder="Harga Tiket" required>
                                </div>
                        
                                <div class="form-group">
                                    <label>Deskripsi</label>    
                                    <textarea name="deskripsi" id="editor1" class="form-control" style="height:500px;"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Link Youtube</label>
                                    <input type="text" class="form-control" name="link_youtube" placeholder="Link Youtube">
                                </div>

                                <div class="form-group">
                                    <label>Link Google Maps</label>
                                    <input type="text" class="form-control" name="google_maps" placeholder="Link Google Maps" required>
                                </div>
                            
                                <?php
                                    if($this->session->userdata("id_level") == 1) {
                                        ?>
                                            <div class="form-group">
                                                <label>Status</label>
                                                <select name="status" id="" class="form-control">
                                                    <option value="">[ Pilih Status ]</option>
                                                    <option value="1">aktif</option>
                                                    <option value="0">tidak aktif</option>
                                                </select>
                                            </div>
                                        <?php
                                    } else {
                                        ?>
                                            <input type="hidden" name="status" value="0">
                                        <?php
                                    }
                                ?>

                            </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->