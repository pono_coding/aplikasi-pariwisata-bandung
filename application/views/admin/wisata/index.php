<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Wisata
            <small>data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('auth/wisata/create') ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Foto</th>
                                    <th>Nama Wisata</th>
                                    <th>Kategori</th>
                                    <th>User</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    foreach($dataWisata as $index => $data) {
                                        ?>
                                            <tr>
                                                <td><?= $index + 1 ?></td>
                                                <td>
                                                    <img src="<?= base_url('uploads/wisata/'.$data->foto_wisata) ?>" alt="" width="100px">
                                                </td>
                                                <td><?= $data->nama_wisata ?></td>
                                                <td><?= $data->kategori_wisata ?></td>
                                                <td><?= $data->user_nama_lengkap ?></td>
                                                <td>
                                                    <?php
                                                        if($this->session->userdata("id_level") == 1) {
                                                            if($data->status_wisata == 0) {
                                                                echo "<label class='label label-danger'>tidak aktif</label>";
                                                            } else {
                                                                echo "<label class='label label-success'>aktif</label>";                                                            
                                                            }
                                                        } else {
                                                            if($data->status_wisata == 0) {
                                                                echo "<label class='label label-warning'>menunggu di acc oleh admin</label>";
                                                            } else {
                                                                echo "<label class='label label-success'>di acc</label>";                                                            
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="<?= site_url('auth/wisata/show/'.$data->id_wisata) ?>" class="btn btn-sm btn-info"><i class="fa fa-th"></i></a>
                                                    <a href="<?= site_url('auth/wisata/edit/'.$data->id_wisata) ?>" class="btn btn-warning btn-sm"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?= site_url('auth/wisata/delete/'.$data->id_wisata) ?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                        <?php
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->