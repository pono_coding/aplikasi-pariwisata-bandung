
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url('assets')?>/dist/img/people.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p><?= $this->session->userdata("username") ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li class="<?= $this->uri->segment(2) == "dashboard" ? "active" : "" ?>"><a href="<?= site_url('auth/dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>        
            <?php
                if($this->session->userdata("id_level") == 1) {
                    ?>
                        <li class="<?= $this->uri->segment(2) == "kategori" ? "active" : "" ?>"><a href="<?= site_url('auth/kategori') ?>"><i class="fa fa-book"></i> <span>Kategori</span></a></li>        
                        <li class="<?= $this->uri->segment(2) == "wisata" ? "active" : "" ?>"><a href="<?= site_url('auth/wisata') ?>"><i class="fa fa-picture-o"></i> <span>Wisata</span></a></li>  
                        <li class="<?= $this->uri->segment(2) == "penginapan" ? "active" : "" ?>"><a href="<?= site_url('auth/penginapan') ?>"><i class="fa fa-home"></i> <span>Penginapan</span></a></li>  
                        <li class="<?= $this->uri->segment(2) == "user" ? "active" : "" ?>"><a href="<?= site_url('auth/user') ?>"><i class="fa fa-users"></i> <span>User</span></a></li>  
                        <li class="<?= $this->uri->segment(2) == "event" ? "active" : "" ?>"><a href="<?= site_url('auth/event') ?>"><i class="fa fa-tasks"></i> <span>Event</span></a></li>  
                        <li class="<?= $this->uri->segment(2) == "artikel" ? "active" : "" ?>"><a href="<?= site_url('auth/artikel') ?>"><i class="fa fa-book"></i> <span>Artikel</span></a></li>  
                   <?php
                   } else {
                       ?>
                            <li class="<?= $this->uri->segment(2) == "wisata" ? "active" : "" ?>"><a href="<?= site_url('auth/wisata') ?>"><i class="fa fa-picture-o"></i> <span>Wisata</span></a></li>  
                       <?php
                   }
            ?>

</ul>
    </section>
    <!-- /.sidebar -->
</aside>