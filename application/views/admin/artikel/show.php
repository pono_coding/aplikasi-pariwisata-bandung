<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Artikel
            <small>detail data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('auth/artikel') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="" class="table table-bordered table-striped">
                            <tr>
                                <th width="200px">Nama Artikel</th>
                                <td width="50px">:</td>
                                <td><?= $artikel->nama ?></td>
                            </tr>

                            <tr>
                                <th>Foto</th>
                                <td>:</td>
                                <td><img src="<?= base_url('uploads/artikel/'.$artikel->foto) ?>" width="200px"/></td>
                            </tr>

                            <tr>
                                <th>Deskripsi</th>
                                <td>:</td>
                                <td><?= $artikel->deskripsi ?></td>
                            </tr>
                        </table>    
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->