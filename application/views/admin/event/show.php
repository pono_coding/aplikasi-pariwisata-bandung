<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Event
            <small>detail data</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        <a href="<?= site_url('auth/event') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                    </div>  
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="" class="table table-bordered table-striped">
                            <tr>
                                <th width="200px">Nama Event</th>
                                <td width="50px">:</td>
                                <td><?= $event->nama ?></td>
                            </tr>

                            <tr>
                                <th>Foto</th>
                                <td>:</td>
                                <td><img src="<?= base_url('uploads/event/'.$event->foto) ?>" width="200px"/></td>
                            </tr>

                            <tr>
                                <th>Deskripsi</th>
                                <td>:</td>
                                <td><?= $event->deskripsi ?></td>
                            </tr>
                        </table>    
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->