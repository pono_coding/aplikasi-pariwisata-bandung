<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
      <h1>
          Event
          <small>Tambah Data</small>
      </h1>
  </section>

  <!-- Main content -->
  <section class="content">
        <div class="row">
            <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box">
                        <div class="box-header with-border">
                            <a href="<?= site_url('auth/event') ?>" class="btn btn-success"><i class="fa fa-chevron-left"></i> Kembali</a>
                        </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                        <form role="form" method="POST" action="<?= site_url('auth/event/store')?>" enctype="multipart/form-data">
                            <div class="box-body">
                                <div class="form-group">
                                    <label>Nama event</label>
                                    <input type="text" class="form-control" name="nama" placeholder="Nama event" required>
                                </div>

                                <div class="form-group">
                                    <label>Foto</label>
                                    <input type="file" class="form-control" name="foto" placeholder="Foto" required>
                                </div>
                        
                                <div class="form-group">
                                    <label>Deskripsi</label>    
                                    <textarea name="deskripsi" id="editor1" class="form-control" style="height:500px;"></textarea>
                                </div>
                            </div>

                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            <!-- /.box -->
            </div>
            <!--/.col (left) -->
        </div>
    <!-- /.row -->
  </section>
  <!-- /.content -->
</div>
  <!-- /.content-wrapper -->