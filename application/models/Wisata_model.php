<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Wisata_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("wisata", $data);
        }

        function read($where = "", $order = "", $limit ="") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($order)) $this->db->order_by($order);
            if(!empty($limit)) $this->db->limit($limit);

            $this->db->select("wisata.id_youtube as id_youtube, wisata.foto_2 as foto_2,wisata.foto_3 as foto_3,wisata.foto_4 as foto_4,wisata.foto_5 as foto_5,wisata.foto_6 as foto_6,wisata.foto_7 as foto_7,wisata.foto_8 as foto_8,wisata.foto_9 as foto_9,wisata.foto_10 as foto_10,wisata.link_youtube as link_youtube,wisata.id as id_wisata,wisata.harga_tiket, wisata.alasan as alasan, wisata.waktu_upload,wisata.nama as nama_wisata, kategori.id as id_kategori_wisata,kategori.nama_kategori as kategori_wisata, user.nama_lengkap as user_nama_lengkap, wisata.status as status_wisata, wisata.deskripsi as deskripsi_wisata, wisata.foto as foto_wisata, wisata.google_maps");
            $this->db->join("kategori", "kategori.id = wisata.id_kategori");
            $this->db->join("user", "user.id = wisata.id_user");

            $query = $this->db->get("wisata");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function readHarga($where = "", $whereIn = "", $order = "", $limit ="") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($where)) $this->db->where_in("id_kategori",$whereIn);
            if(!empty($order)) $this->db->order_by($order);
            if(!empty($limit)) $this->db->limit($limit);

            $this->db->select("wisata.id_youtube as id_youtube,wisata.foto_2 as foto_2,wisata.foto_3 as foto_3,wisata.foto_4 as foto_4,wisata.foto_5 as foto_5,wisata.foto_6 as foto_6,wisata.foto_7 as foto_7,wisata.foto_8 as foto_8,wisata.foto_9 as foto_9,wisata.foto_10 as foto_10, wisata.deskripsi as deskripsi_wisata, wisata.link_youtube as link_youtube, wisata.id as id_wisata,wisata.id_user as wisata_id_user,wisata.alasan as alasan, wisata.status as status,wisata.harga_tiket, wisata.waktu_upload,wisata.nama as nama_wisata, kategori.id as id_kategori_wisata,kategori.nama_kategori as kategori_wisata, user.nama_lengkap as user_nama_lengkap, wisata.status as status_wisata, wisata.deskripsi as deskripsi_wisata, wisata.foto as foto_wisata");
            $this->db->join("kategori", "kategori.id = wisata.id_kategori");
            $this->db->join("user", "user.id = wisata.id_user");

            $query = $this->db->get("wisata");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("wisata", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("wisata");
        }
    }
?>