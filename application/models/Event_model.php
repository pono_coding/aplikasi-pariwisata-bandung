<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Event_model extends CI_Model 
    {
        function create($data) 
        {
            $this->db->insert("event", $data);
        }

        function read($where = "", $order = "", $limit = "") 
        {
            if(!empty($where)) $this->db->where($where);
            if(!empty($order)) $this->db->order_by($order);
            if(!empty($limit)) $this->db->limit($limit);

            $query = $this->db->get("event");

            if($query AND $query->num_rows() != 0) {
                return $query->result();
            } else {
                return array();
            }
        }

        function update($id, $data)
        {
            $this->db->where($id);
            $this->db->update("event", $data);
        }

        function delete($id)
        {
            $this->db->where($id);
            $this->db->delete("event");
        }
    }
?>